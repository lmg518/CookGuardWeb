class SSubsysListsController < ApplicationController
  before_filter :authenticate_user
  before_action :set_s_subsys_liste, only: [:show, :edit, :update, :destroy]

  # GET /s_subsys_listes
  # GET /user_subsys_lists.json
  def user_subsys
    
    # 业务系统链接
    @s_subsys_lists = current_user.s_subsys_lists
    
    respond_to do |format|
      #format.html { redirect_to "/sessions/new", notice: 'Session was successfully destroyed.' }
      format.json { render :user_subsys, status: :ok }
    end
  end
  
  # GET /s_subsys_listes
  # GET /s_subsys_listes.json
  def index
    @s_subsys_lists = SSubsysList.all
    # @s_subsys_lists.each do |subsys|
    #   Rails.logger.info subsys.sys_name
    #         Rails.logger.info '===================='
    # end

  end

  # GET /s_subsys_listes/1
  # GET /s_subsys_listes/1.json
  def show
  end

  # GET /s_subsys_listes/new
  def new
    @s_subsys_list = SSubsysList.new
  end

  # GET /s_subsys_listes/1/edit
  def edit
  end

  # POST /s_subsys_listes
  # POST /s_subsys_listes.json
  def create
    @s_subsys_list = SSubsysList.new(s_subsys_list_params)

    respond_to do |format|
      if @s_subsys_liste.save
        format.html { redirect_to @s_subsys_list, notice: 'S subsys liste was successfully created.' }
        format.json { render :show, status: :created, location: @s_subsys_list }
      else
        format.html { render :new }
        format.json { render json: @s_subsys_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /s_subsys_listes/1
  # PATCH/PUT /s_subsys_listes/1.json
  def update
    respond_to do |format|
      if @s_subsys_liste.update(s_subsys_list_params)
        format.html { redirect_to @s_subsys_list, notice: 'S subsys liste was successfully updated.' }
        format.json { render :show, status: :ok, location: @s_subsys_list }
      else
        format.html { render :edit }
        format.json { render json: @s_subsys_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /s_subsys_listes/1
  # DELETE /s_subsys_listes/1.json
  def destroy
    @s_subsys_liste.destroy
    respond_to do |format|
      format.html { redirect_to s_subsys_lists_url, notice: 'S subsys liste was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_s_subsys_list
      @s_subsys_list = SSubsysList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def s_subsys_liste_params
      params.require(:s_subsys_list).permit(:sys_name, :sys_url, :mark)
    end
end
