desc "河南气象站"
task "henan_wether" => :environment do
 [
     '53889,林州',
     '53898,安阳',
     '53972,沁阳',
     '53974,淇县',
     '53978,济源',
     '53979,博爱',
     '53982,焦作',
     '53983,封丘',
     '53984,修武',
     '53985,辉县',
     '53986,新乡',
     '53987,武陟',
     '53988,获嘉',
     '53989,原阳',
     '53991,汤阴',
     '53992,浚县',
     '53993,内黄',
     '53994,卫辉',
     '53995,滑县',
     '53997,延津',
     '53998,长垣',
     '54817,台前',
     '54900,濮阳',
     '54901,南乐',
     '54902,清丰',
     '54903,范县',
     '57051,三门峡',
     '57056,灵宝',
     '57063,渑池',
     '57066,洛宁',
     '57070,新安',
     '57071,孟津',
     '57072,孟州',
     '57074,伊川',
     '57075,汝州',
     '57076,偃师',
     '57078,汝阳',
     '57079,温县',
     '57080,巩义',
     '57081,荥阳',
     '57082,登封',
     '57083,郑州',
     '57084,嵩山',
     '57085,新密',
     '57086,新郑',
     '57087,长葛',
     '57088,禹州',
     '57089,许昌',
     '57090,中牟',
     '57091,开封',
     '57093,兰考',
     '57094,尉氏',
     '57095,鄢陵',
     '57096,杞县',
     '57098,扶沟',
     '57099,太康',
     '57156,西峡',
     '57162,嵩县',
     '57169,内乡',
     '57173,鲁山',
     '57175,镇平',
     '57176,南召',
     '57177,舞钢',
     '57178,南阳',
     '57179,方城',
     '57180,郏县',
     '57181,宝丰',
     '57182,襄城',
     '57183,临颍',
     '57184,叶县',
     '57185,舞阳',
     '57186,漯河',
     '57187,社旗',
     '57188,西平',
     '57189,遂平',
     '57190,黄泛区农试站',
     '57191,通许',
     '57192,淮阳',
     '57193,西华',
     '57194,上蔡',
     '57195,川汇区',
     '57196,项城',
     '57197,汝南',
     '57198,商水',
     '57261,淅川',
     '57271,新野',
     '57273,唐河',
     '57274,邓州',
     '57281,泌阳',
     '57285,桐柏',
     '57290,驻马店',
     '57292,平舆',
     '57293,新蔡',
     '57294,确山',
     '57295,正阳',
     '57296,息县',
     '57297,信阳',
     '57298,罗山',
     '57299,光山',
     '57390,鸡公山',
     '57396,新县',
     '58001,睢县',
     '58004,民权',
     '58005,商丘',
     '58006,虞城',
     '58007,柘城',
     '58008,宁陵',
     '58017,夏邑',
     '58100,郸城',
     '58101,鹿邑',
     '58104,沈丘',
     '58111,永城',
     '58205,淮滨',
     '58207,潢川',
     '58208,固始',
     '58301,商城',
 ].each do |station|
   arr = station.split(",")
   DWeatherStation.create(:station_id => arr[0].to_i, :station_name => arr[1])
 end

end