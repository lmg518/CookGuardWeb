class UserV1API < Grape::API
  format :json

  helpers do
    def current_user
      token = headers['Authentication-Token']
      @current_user = DLoginMsg.find_by(session: token)
    rescue
      error!('401 Unauthorized', 401)
    end

    def authenticate!
      error!('401 Unauthorized', 401) unless current_user
    end
  end

  desc "用户登陆"
  params do
    requires :user_no, type: String, desc: "用户名"
    requires :user_pw, type: String, desc: "用户密码"
  end
  post '/' do
    @user = DLoginMsg.find_by(:login_no => params[:user_no], "valid_flag" => "1")
    if @user.nil?
      error!('401 Unauthorized', 401)
    end
    unless @user.authenticate(params[:user_pw])
      error!('401 Unauthorized', 401)
    end
    puts request.ip
    if @user.update(:session => @user.new_session, :ip_address => env['action_dispatch.remote_ip'].to_s)
      role = @user.s_role_msg
      {user: @user.attributes.slice("id", "session", "login_name", "login_no"),role: role.present? ? role.role_name : ""}
    else
      error!('401 Unauthorized', 401)
    end
  end

  desc "用户退出", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }

  post '/sign_out' do
    authenticate!
    begin
      @current_user.update(:session => "---------------")
      session[:session_code] = "----"
      return {message: "用户已退出", status: 201}
    rescue Exception => e
      puts e
      return error!('401 Unauthorized', 401)
    end
  end

  desc "用户regist_id", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    requires :regist_id, :type => String, desc: "uuid"
  end
  post '/regist' do
    authenticate!
    if @current_user.update(:registration_id => params[:regist_id])
      return {user: @current_user.attributes.slice("id", "session", "login_name", "login_no", "registration_id")}
    else
      return {msg: @current_user.errors}
    end
  end

  desc "用户权限", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  post "/role_infos" do
    authenticate!
    role = @current_user.s_role_msg
    role_funcs = role.s_role_funcs
    func_hash = []
    role_funcs.map {|x| list = SSubsystList.find_by(:id => x.s_func_msg_id);
    if list.present?;
      func_hash << list;
    end;}
    func_hash
  end

end