class DLoginMsg < ActiveRecord::Base
  has_secure_password

  belongs_to :s_role_msg #, foreign_key: "role_id"
  belongs_to :s_region_code, foreign_key: "group_id"
  belongs_to :s_region_code_info

  # 按名字模糊查询
  scope :by_user_name, ->(user_name){
    where("login_name like ?", "%#{user_name}%") if user_name.present?
  }

  # 按名字模糊查询
  scope :by_user_id, ->(role_id){
    where(:s_role_msg_id => role_id) if role_id.present?
  }

  def new_session
    SecureRandom.urlsafe_base64
  end
  
  def navi_bar
    self.s_role_msg.s_func_msgs.where( "root_distance = 1 and valid_flag = 0" ).order( :queue_index )
  end
  
  #### 工号所属区域及子区
  def region_codes
    @d_login_region_codes    = Array.new
    @d_login_region_codes[0] = self.s_region_code.region_code
    self.s_region_code.son_region.each do | son_region_code |
      @d_login_region_codes.push( son_region_code.region_code )
    end
    
    return @d_login_region_codes
  end
  
  
end
