class SRegionCode < ActiveRecord::Base
  has_many :son_region, class_name: "SRegionCode", foreign_key: "father_region_id" 
  belongs_to :father_region, class_name: "SRegionCode"
  has_many :d_login_msgs, foreign_key: "group_id"
  has_many :d_sms_notices, foreign_key: "group_id"
  has_many :s_region_code_infos
  
  def d_stations
    DStation.where( "region_code = ? ", self.region_code )
  end
  
  def g_stations
    GStation.where( "region_code = ? ", self.region_code )
  end
  
  #### 区域浓度值时间对比环比、同比
  def county_thickness_compares( start_datetime, end_datetime, source_data, station_type )
    @sequential_data = Array.new
    if start_datetime.nil? && end_datetime.nil?
      # if source_data.nil?
      #   ###  获取前一天数据   sequential 环比  compared 同比
      #   daily = DDataDaily.where( "data_time = (CURRENT_DATE - 1)").where( "station_id" => self.d_stations.pluck("id") )
      #   ###  获取上月前一天数据
      #   daily_seq = DDataDaily.where( "data_time = (CURRENT_DATE - INTERVAL 1 MONTH)-1" ).where( "station_id" => self.d_stations.pluck("id") )
      #   ###  获取上年前一天数据
      #   daily_com = DDataDaily.where( "data_time = (CURRENT_DATE - INTERVAL 1 YEAR)-1" ).where( "station_id" => self.d_stations.pluck("id") )
      # elsif source_data.eql?("initial_data")
      #   ###  获取前一天数据   sequential 环比  compared 同比
      #   daily = DDataDaily.where( "data_time = (CURRENT_DATE - 1)" ).where( "station_id" => self.d_stations.pluck("id") )
      #   ###  获取上月前一天数据
      #   daily_seq = DDataDaily.where( "data_time = (CURRENT_DATE - INTERVAL 1 MONTH)-1" ).where( "station_id" => self.d_stations.pluck("id") )
      #   ###  获取上年前一天数据
      #   daily_com = DDataDaily.where( "data_time = (CURRENT_DATE - INTERVAL 1 YEAR)-1" ).where( "station_id" => self.d_stations.pluck("id") )
      # elsif source_data.eql?("audit_data")
      #   ###  获取前一天数据   sequential 环比  compared 同比
      #   daily = DAuditDataDaily.where( "data_time = (CURRENT_DATE - 1)" ).where( "station_id" => self.d_stations.pluck("id") )
      #   ###  获取上月前一天数据
      #   daily_seq = DAuditDataDaily.where( "data_time = (CURRENT_DATE - INTERVAL 1 MONTH)-1" ).where( "station_id" => self.d_stations.pluck("id") )
      #   ###  获取上年前一天数据
      #   daily_com = DAuditDataDaily.where( "data_time = (CURRENT_DATE - INTERVAL 1 YEAR)-1" ).where( "station_id" => self.d_stations.pluck("id") )
      # end
      # ###  环比数据计算
      # if daily.length == 0 || daily_seq.length == 0
        seq_pm10 = '-'
        seq_so2  = '-'
        seq_no2  = '-'
        seq_co   = '-'
        seq_o3   = '-'
        seq_pm25 = '-'
      #   Rails.logger.debug( "数据为空" )
      # else
      #   Rails.logger.debug( "数据不为空" )
      #   if daily.average( :avg_pm10 ).nil? || daily_seq.average( :avg_pm10 ).nil? || daily_seq.average( :avg_pm10 ) == 0
      #     seq_pm10 = '-'
      #   else
      #     seq_pm10 = eval(sprintf("%8.1f",(daily.average( :avg_pm10 ) - daily_seq.average( :avg_pm10 )) * 1.0000/daily_seq.average( :avg_pm10 ) * 100))
      #   end
      #   if daily.average( :avg_so2 ).nil? || daily_seq.average( :avg_so2 ).nil? || daily_seq.average( :avg_so2 ) == 0
      #     seq_so2  = '-'
      #   else
      #     seq_so2  = eval(sprintf("%8.1f",(daily.average( :avg_so2 ) - daily_seq.average( :avg_so2 )) * 1.0000/daily_seq.average( :avg_so2 ) * 100))
      #   end
      #   if daily.average( :avg_no2 ).nil? || daily_seq.average( :avg_no2 ).nil? || daily_seq.average( :avg_no2 ) == 0
      #     seq_no2  = '-'
      #   else
      #     seq_no2  = eval(sprintf("%8.1f",(daily.average( :avg_no2 ) - daily_seq.average( :avg_no2 )) * 1.0000/daily_seq.average( :avg_no2 ) * 100))
      #   end
      #   if daily.average( :avg_co ).nil? || daily_seq.average( :avg_co ).nil? || daily_seq.average( :avg_co ) == 0
      #     seq_co   = '-'
      #   else
      #     seq_co   = eval(sprintf("%8.1f",(daily.average( :avg_co ) - daily_seq.average( :avg_co )) * 1.0000/daily_seq.average( :avg_co ) * 100))
      #   end
      #   if daily.average( :max_avg_1h_o3 ).nil? || daily_seq.average( :max_avg_1h_o3 ).nil? || daily_seq.average( :max_avg_1h_o3 ) == 0
      #     seq_o3   = '-'
      #   else
      #     seq_o3   = eval(sprintf("%8.1f",(daily.average( :max_avg_1h_o3 ) - daily_seq.average( :max_avg_1h_o3 )) * 1.0000/daily_seq.average( :max_avg_1h_o3 ) * 100))
      #   end
      #   if daily.average( :avg_pm25 ).nil? || daily_seq.average( :avg_pm25 ).nil? || daily_seq.average( :avg_pm25 ) == 0
      #     seq_pm25 = '-'
      #   else
      #     seq_pm25 = eval(sprintf("%8.1f",(daily.average( :avg_pm25 ) - daily_seq.average( :avg_pm25 )) * 1.0000/daily_seq.average( :avg_pm25 ) * 100))
      #   end
      # end
      # ###  同比数据计算
      # if daily.length == 0 || daily_com.length == 0
        com_pm10 = '-'
        com_so2  = '-'
        com_no2  = '-'
        com_co   = '-'
        com_o3   = '-'
        com_pm25 = '-'
      # else
      #   if daily.average( :avg_pm10 ).nil? || daily_com.average( :avg_pm10 ).nil? || daily_com.average( :avg_pm10 ) == 0
      #     com_pm10 = '-'
      #   else
      #     com_pm10 = eval(sprintf("%8.1f",(daily.average( :avg_pm10 ) - daily_com.average( :avg_pm10 )) * 1.0000/daily_com.average( :avg_pm10 ) * 100))
      #   end
      #   if daily.average( :avg_so2 ).nil? || daily_com.average( :avg_so2 ).nil? || daily_com.average( :avg_so2 ) == 0
      #     com_so2  = '-'
      #   else
      #     com_so2  = eval(sprintf("%8.1f",(daily.average( :avg_so2 ) - daily_com.average( :avg_so2 )) * 1.0000/daily_com.average( :avg_so2 ) * 100))
      #   end
      #   if daily.average( :avg_no2 ).nil? || daily_com.average( :avg_no2 ).nil? || daily_com.average( :avg_no2 ) == 0
      #     com_no2  = '-'
      #   else
      #     com_no2  = eval(sprintf("%8.1f",(daily.average( :avg_no2 ) - daily_com.average( :avg_no2 )) * 1.0000/daily_com.average( :avg_no2 ) * 100))
      #   end
      #   if daily.average( :avg_co ).nil? || daily_com.average( :avg_co ).nil? || daily_com.average( :avg_co ) == 0
      #     com_co   = '-'
      #   else
      #     com_co   = eval(sprintf("%8.1f",(daily.average( :avg_co ) - daily_com.average( :avg_co )) * 1.0000/daily_com.average( :avg_co ) * 100))
      #   end
      #   if daily.average( :max_avg_1h_o3 ).nil? || daily_com.average( :max_avg_1h_o3 ).nil? || daily_com.average( :max_avg_1h_o3 ) == 0
      #     com_o3   = '-'
      #   else
      #     com_o3   = eval(sprintf("%8.1f",(daily.average( :max_avg_1h_o3 ) - daily_com.average( :max_avg_1h_o3 )) * 1.0000/daily_com.average( :max_avg_1h_o3 ) * 100))
      #   end
      #   if daily.average( :avg_pm25 ).nil? || daily_com.average( :avg_pm25 ).nil? || daily_com.average( :avg_pm25 ) == 0
      #     com_pm25 = '-'
      #   else
      #     com_pm25 = eval(sprintf("%8.1f",(daily.average( :avg_pm25 ) - daily_com.average( :avg_pm25 )) * 1.0000/daily_com.average( :avg_pm25 ) * 100))
      #   end
      # end
      # ### 当前是否有数据
      # if daily.length != 0
      #   daily_pm10 = daily.average( :avg_pm10 ).to_i
      #   daily_so2  = daily.average( :avg_so2 ).to_i
      #   daily_no2  = daily.average( :avg_no2 ).to_i
      #   daily_co   = eval(sprintf("%8.1f",daily.average( :avg_co )))
      #   daily_o3   = daily.average( :max_avg_1h_o3 ).to_i
      #   daily_pm25 = daily.average( :avg_pm25 ).to_i
      # else
        daily_pm10 = '-'
        daily_so2  = '-'
        daily_no2  = '-'
        daily_co   = '-'
        daily_o3   = '-'
        daily_pm25 = '-'
      # end
      # ### 上月是否有数据
      # if daily_seq.length != 0
      #   daily_seq_pm10 = daily_seq.average( :avg_pm10 ).to_i
      #   daily_seq_so2  = daily_seq.average( :avg_so2 ).to_i
      #   daily_seq_no2  = daily_seq.average( :avg_no2 ).to_i
      #   daily_seq_co   = eval(sprintf("%8.1f",daily_seq.average( :avg_co )))
      #   daily_seq_o3   = daily_seq.average( :max_avg_1h_o3 ).to_i
      #   daily_seq_pm25 = daily_seq.average( :avg_pm25 ).to_i
      # else
        daily_seq_pm10 = '-'
        daily_seq_so2  = '-'
        daily_seq_no2  = '-'
        daily_seq_co   = '-'
        daily_seq_o3   = '-'
        daily_seq_pm25 = '-'
      # end
      # ### 上年是否有数据
      # if daily_com.length != 0
      #   daily_com_pm10 = daily_com.average( :avg_pm10 ).to_i
      #   daily_com_so2  = daily_com.average( :avg_so2 ).to_i
      #   daily_com_no2  = daily_com.average( :avg_no2 ).to_i
      #   daily_com_co   = eval(sprintf("%8.1f",daily_com.average( :avg_co )))
      #   daily_com_o3   = daily_com.average( :max_avg_1h_o3 ).to_i
      #   daily_com_pm25 = daily_com.average( :avg_pm25 ).to_i
      # else
        daily_com_pm10 = '-'
        daily_com_so2  = '-'
        daily_com_no2  = '-'
        daily_com_co   = '-'
        daily_com_o3   = '-'
        daily_com_pm25 = '-'
      # end
      # ###  数据存储数组
      @sequential_data = [daily_pm10,daily_so2,daily_no2,daily_co,daily_o3,daily_pm25,daily_seq_pm10,daily_seq_so2,daily_seq_no2,daily_seq_co,daily_seq_o3,daily_seq_pm25,seq_pm10,seq_so2,seq_no2,seq_co,seq_o3,seq_pm25,daily_com_pm10,daily_com_so2,daily_com_no2,daily_com_co,daily_com_o3,daily_com_pm25,com_pm10,com_so2,com_no2,com_co,com_o3,com_pm25]
    else
      if source_data.nil?
        daily     = DDataDaily.where( "data_time <= ? and data_time >= ? ", end_datetime.delete( "-" ), start_datetime.delete( "-" ) ).where( "station_id" => self.d_stations.s_by_types(station_type).select( "id" ).distinct )
        daily_seq = DDataDaily.where( "data_time <= ? and data_time >= ? ", Date.parse(end_datetime).months_since(-1).strftime( '%Y%m%d' ), Date.parse(start_datetime).months_since(-1).strftime( '%Y%m%d' ) ).where( "station_id" => self.d_stations.s_by_types(station_type).select( "id" ).distinct )
        daily_com = DDataDaily.where( "data_time <= ? and data_time >= ? ", Date.parse(end_datetime).years_since(-1).strftime( '%Y%m%d' ), Date.parse(start_datetime).years_since(-1).strftime( '%Y%m%d' ) ).where( "station_id" => self.d_stations.s_by_types(station_type).select( "id" ).distinct )
      elsif source_data.eql?("initial_data")
        daily     = DDataDaily.where( "data_time <= ? and data_time >= ? ", end_datetime.delete( "-" ), start_datetime.delete( "-" ) ).where( "station_id" => self.d_stations.s_by_types(station_type).select( "id" ).distinct )
        daily_seq = DDataDaily.where( "data_time <= ? and data_time >= ? ", Date.parse(end_datetime).months_since(-1).strftime( '%Y%m%d' ), Date.parse(start_datetime).months_since(-1).strftime( '%Y%m%d' ) ).where( "station_id" => self.d_stations.s_by_types(station_type).select( "id" ).distinct )
        daily_com = DDataDaily.where( "data_time <= ? and data_time >= ? ", Date.parse(end_datetime).years_since(-1).strftime( '%Y%m%d' ), Date.parse(start_datetime).years_since(-1).strftime( '%Y%m%d' ) ).where( "station_id" => self.d_stations.s_by_types(station_type).select( "id" ).distinct )
      elsif source_data.eql?("audit_data")
        daily     = DAuditDataDaily.where( "data_time <= ? and data_time >= ? ", end_datetime.delete( "-" ), start_datetime.delete( "-" ) ).where( "station_id" => self.d_stations.s_by_types(station_type).select( "id" ).distinct )
        daily_seq = DAuditDataDaily.where( "data_time <= ? and data_time >= ? ", Date.parse(end_datetime).months_since(-1).strftime( '%Y%m%d' ), Date.parse(start_datetime).months_since(-1).strftime( '%Y%m%d' ) ).where( "station_id" => self.d_stations.s_by_types(station_type).select( "id" ).distinct )
        daily_com = DAuditDataDaily.where( "data_time <= ? and data_time >= ? ", Date.parse(end_datetime).years_since(-1).strftime( '%Y%m%d' ), Date.parse(start_datetime).years_since(-1).strftime( '%Y%m%d' ) ).where( "station_id" => self.d_stations.s_by_types(station_type).select( "id" ).distinct )
      end
      ###  环比数据计算
      if daily.length == 0 || daily_seq.length == 0
        seq_pm10 = '-'
        seq_so2  = '-'
        seq_no2  = '-'
        seq_co   = '-'
        seq_o3   = '-'
        seq_pm25 = '-'
        Rails.logger.debug( "数据为空" )
      else
        Rails.logger.debug( "数据不为空" )
        if daily_seq.average( :avg_pm10 ) != 0 
          seq_pm10 = eval(sprintf("%8.1f",(daily.average( :avg_pm10 ) - daily_seq.average( :avg_pm10 )) * 1.0000/daily_seq.average( :avg_pm10 ) * 100))
        else
          seq_pm10 = '-'
        end
        if daily_seq.average( :avg_so2 ) != 0
          seq_so2  = eval(sprintf("%8.1f",(daily.average( :avg_so2 ) - daily_seq.average( :avg_so2 )) * 1.0000/daily_seq.average( :avg_so2 ) * 100))
        else
          seq_so2  = '-'
        end
        if daily_seq.average( :avg_no2 ) != 0
          seq_no2  = eval(sprintf("%8.1f",(daily.average( :avg_no2 ) - daily_seq.average( :avg_no2 )) * 1.0000/daily_seq.average( :avg_no2 ) * 100))
        else
          seq_no2  = '-'
        end
        if daily_seq.average( :avg_co ) != 0
          seq_co   = eval(sprintf("%8.1f",(daily.average( :avg_co ) - daily_seq.average( :avg_co )) * 1.0000/daily_seq.average( :avg_co ) * 100))
        else
          seq_co   = '-'
        end
        if daily_seq.average( :max_avg_1h_o3 ) != 0
          seq_o3   = eval(sprintf("%8.1f",(daily.average( :max_avg_1h_o3 ) - daily_seq.average( :max_avg_1h_o3 )) * 1.0000/daily_seq.average( :max_avg_1h_o3 ) * 100))
        else
          seq_o3   = '-'
        end  
        if daily_seq.average( :avg_pm25 ) != 0
          seq_pm25 = eval(sprintf("%8.1f",(daily.average( :avg_pm25 ) - daily_seq.average( :avg_pm25 )) * 1.0000/daily_seq.average( :avg_pm25 ) * 100))
        else
          seq_pm25 = '-'
        end
      end 
      
      ###  同比数据计算
      if daily.length == 0 || daily_com.length == 0 
        com_pm10 = '-'
        com_so2  = '-'
        com_no2  = '-'
        com_co   = '-'
        com_o3   = '-'
        com_pm25 = '-'
      else
        if daily.average( :avg_pm10 ).nil? || daily_com.average( :avg_pm10 ).nil? || daily_com.average( :avg_pm10 ) == 0
          com_pm10 = '-'
        else
          com_pm10 = eval(sprintf("%8.1f",(daily.average( :avg_pm10 ) - daily_com.average( :avg_pm10 )) * 1.0000/daily_com.average( :avg_pm10 ) * 100))
        end
        if daily.average( :avg_so2 ).nil? || daily_com.average( :avg_so2 ).nil? || daily_com.average( :avg_so2 ) == 0
          com_so2  = '-'
        else  
          com_so2  = eval(sprintf("%8.1f",(daily.average( :avg_so2 ) - daily_com.average( :avg_so2 )) * 1.0000/daily_com.average( :avg_so2 ) * 100))
        end
        if daily.average( :avg_no2 ).nil? || daily_com.average( :avg_no2 ).nil? || daily_com.average( :avg_no2 ) == 0
          com_no2  = '-'
        else
          com_no2  = eval(sprintf("%8.1f",(daily.average( :avg_no2 ) - daily_com.average( :avg_no2 )) * 1.0000/daily_com.average( :avg_no2 ) * 100))
        end
        if daily.average( :avg_co ).nil? || daily_com.average( :avg_co ).nil? || daily_com.average( :avg_co ) == 0
          com_co  = '-'
        else
          com_co  = eval(sprintf("%8.1f",(daily.average( :avg_co ) - daily_com.average( :avg_co )) * 1.0000/daily_com.average( :avg_co ) * 100))
        end
        if daily.average( :max_avg_1h_o3 ).nil? || daily_com.average( :max_avg_1h_o3 ).nil? || daily_com.average( :max_avg_1h_o3 ) == 0
          com_o3  = '-'
        else
          com_o3  = eval(sprintf("%8.1f",(daily.average( :max_avg_1h_o3 ) - daily_com.average( :max_avg_1h_o3 )) * 1.0000/daily_com.average( :max_avg_1h_o3 ) * 100))
        end
        if daily.average( :avg_pm25 ).nil? || daily_com.average( :avg_pm25 ).nil? || daily_com.average( :avg_pm25 ) == 0
          com_pm25  = '-'
        else
          com_pm25  = eval(sprintf("%8.1f",(daily.average( :avg_pm25 ) - daily_com.average( :avg_pm25 )) * 1.0000/daily_com.average( :avg_pm25 ) * 100))
        end    
      end 
      
      ### 当前是否有数据
      if daily.length != 0
        daily_pm10 = daily.average( :avg_pm10 ).to_i
        daily_so2  = daily.average( :avg_so2 ).to_i
        daily_no2  = daily.average( :avg_no2 ).to_i
        if daily.average( :avg_co )
          daily_co   = eval(sprintf("%8.1f",daily.average( :avg_co )))
        else
          daily_co   = 0
        end
        daily_o3   = daily.average( :max_avg_1h_o3 ).to_i
        daily_pm25 = daily.average( :avg_pm25 ).to_i
      else
        daily_pm10 = '-'
        daily_so2  = '-'
        daily_no2  = '-'
        daily_co   = '-'
        daily_o3   = '-'
        daily_pm25 = '-'
      end 
      
      ### 上月是否有数据
      if daily_seq.length != 0
        daily_seq_pm10 = daily_seq.average( :avg_pm10 ).to_i
        daily_seq_so2  = daily_seq.average( :avg_so2 ).to_i
        daily_seq_no2  = daily_seq.average( :avg_no2 ).to_i
        if daily.average( :avg_co )
          daily_seq_co   = eval(sprintf("%8.1f",daily_seq.average( :avg_co )))
        else
          daily_seq_co   = 0
        end
        daily_seq_o3   = daily_seq.average( :max_avg_1h_o3 ).to_i
        daily_seq_pm25 = daily_seq.average( :avg_pm25 ).to_i
      else
        daily_seq_pm10 = '-'
        daily_seq_so2  = '-'
        daily_seq_no2  = '-'
        daily_seq_co   = '-'
        daily_seq_o3   = '-'
        daily_seq_pm25 = '-'
      end  
      
      ### 上年是否有数据
      if daily_com.length != 0
        daily_com_pm10 = daily_com.average( :avg_pm10 ).to_i
        daily_com_so2  = daily_com.average( :avg_so2 ).to_i
        daily_com_no2  = daily_com.average( :avg_no2 ).to_i
        if daily.average( :avg_co )
          daily_com_co   = eval(sprintf("%8.1f",daily_com_co.average( :avg_co )))
        else
          daily_com_co   = 0
        end
        daily_com_o3   = daily_com.average( :max_avg_1h_o3 ).to_i
        daily_com_pm25 = daily_com.average( :avg_pm25 ).to_i
      else
        daily_com_pm10 = '-'
        daily_com_so2  = '-'
        daily_com_no2  = '-'
        daily_com_co   = '-'
        daily_com_o3   = '-'
        daily_com_pm25 = '-'
      end         
      ###  数据存储数组
      @sequential_data = [daily_pm10,daily_so2,daily_no2,daily_co,daily_o3,daily_pm25,daily_seq_pm10,daily_seq_so2,daily_seq_no2,daily_seq_co,daily_seq_o3,daily_seq_pm25,seq_pm10,seq_so2,seq_no2,seq_co,seq_o3,seq_pm25,daily_com_pm10,daily_com_so2,daily_com_no2,daily_com_co,daily_com_o3,daily_com_pm25,com_pm10,com_so2,com_no2,com_co,com_o3,com_pm25]
  
    end
  end
  
end
