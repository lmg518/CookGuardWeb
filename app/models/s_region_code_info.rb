class SRegionCodeInfo < ActiveRecord::Base
    has_many :d_login_mgs
    belongs_to :s_region_codes

    #分页
    paginates_per 10

    #按照单位id查询
    scope :by_region_info, ->(region_id){
        where(:id => region_id) if region_id.present?
    }

    # 按单位名称查询
    scope :by_info_name,->(u_name){
      where("unit_name like ?","%#{u_name}%") if u_name.present?
    }


end