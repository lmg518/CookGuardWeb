class SRoleFunc < ActiveRecord::Base
  belongs_to :s_role_msg
  belongs_to :s_func_msg
  belongs_to :s_subsyst_list, foreign_key: 's_func_msg_id'
end
