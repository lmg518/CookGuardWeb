class SFuncMsg < ActiveRecord::Base
  has_many :son_func, class_name: "SFuncMsg", foreign_key: "father_func_code"
  belongs_to :father_func, class_name: "SFuncMsg"
  
  has_many :s_role_funcs
  has_many :s_role_msgs, through: :s_role_funcs

  ### 查询功能点
  def son_funcs(*user_role_id)
    if user_role_id.length == 0
      SFuncMsg.where( "father_func_code = ? and valid_flag = 0", self.func_code ).order( :queue_index )
    else
      SFuncMsg.joins( "inner join s_role_funcs on s_func_msgs.id = s_role_funcs.s_func_msg_id" ).where( "s_role_funcs.s_role_msg_id = ? and s_func_msgs.father_func_code = ? and s_func_msgs.valid_flag = 0", user_role_id,self.func_code ).order( :queue_index )
    end
  end
end
