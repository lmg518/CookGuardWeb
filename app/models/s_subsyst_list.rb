class SSubsystList < ActiveRecord::Base
  # has_many :s_role_systs
  belongs_to :s_role_msg

  
  #has_many :s_role_subsysts
  #has_many :s_role_msg, :through =>  :s_role_subsysts
  has_many :s_role_funcs ,foreign_key: 's_func_msg_id'
  has_many :s_role_msg, :through =>  :s_role_funcs


  # 应急报警  "air_police"
  # 水质监测  "water_monitor"
  # 污染源监测 "polluter_monitor"
  # 空气监测 "air_monitor"
  # 运维管理 "air_queen"
end
