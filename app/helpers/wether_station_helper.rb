module WetherStationHelper

  #auther: dingfj
  # 每小时43分时拉取数据并存DWetherDatum数据
  def self.init
    @station_arr = []
    @station__false = []
    @count = 0
    w_stations = DWeatherStation.all.order(:id => :asc)
    @w_station_id_arr = w_stations.pluck(:station_id)
    arr_length = @w_station_id_arr.length
    x = arr_length % 30
    if 0 == x
      new_times = arr_length / 30
      new_times.times do |i|
        a = i * 30 + 30 - 30
        new_station_ids = @w_station_id_arr[a, 30]
        print "new_station_ids<<<<#{new_station_ids}", "\n"
        get_url(new_station_ids.join(",")) if new_station_ids.present?
      end
    else
      print "????????", "\n"
      new_times = arr_length / 30
      last_times = arr_length % 30
      @a = 0
      new_times.times do |i|
        @a = i * 30 + 30 - 30
        new_station_ids = @w_station_id_arr[@a, 30]
        get_url(new_station_ids.join(",")) if new_station_ids.present?
      end
      last_station_ids = @w_station_id_arr[@a + 30, 30] #  剩余不足30个站点的id
      get_url(last_station_ids.join(",")) if last_station_ids.present?
    end

# 用户名: 5155692512900vTrq
# 密码: DjBOQfc
  end

  def self.get_url(station_ids)
    time = Time.now.utc
    @begin_time = time.strftime("%Y%m%d%H0000")
    @end_time = time.strftime("%Y%m%d%H5959")
    @user = '5155692512900vTrq'
    @pwd = 'DjBOQfc'
    @item = 'WIN_D_INST_Max,WIN_S_Inst_Max,RHU,TEM,PRE_1h'
    @url = "http://api.data.cma.cn:8090/api"
    @response = RestClient.get @url, {params: {
        userId: @user,
        pwd: @pwd,
        dataFormat: 'json',
        interfaceId: 'getSurfEleByTimeRangeAndStaID',
        dataCode: 'SURF_CHN_MUL_HOR',
        timeRange: "[#{@begin_time},#{@begin_time}]",
        staIDs: station_ids,
        elements: "Station_Id_C,Year,Mon,Day,Hour,#{@item}"
    }}
    get_request
  end

  def self.get_request
    json = JSON.parse(@response.body)
    print ">>#{json}", "\n"
    returnCode = json["returnCode"]
    if "0" == returnCode
      #"fieldNames"=>"区站号(字符) 年 月 日 时 极大风速的风向 极大风速 相对湿度 温度/气温 过去1小时降水量",
      # "fieldUnits"=>"- 年 月 日 时 度 米/秒 百分率 摄氏度(℃) 毫米",
      # WIN_D_INST_Max	   极大风速的风向(角度)
      # WIN_S_Inst_Max	   极大风速(米/秒)
      # RHU 相对湿度(%)
      # TEM	温度/气温	摄氏度(℃)
      # PRE_1h 降雨(mm)
      ds_data = json["DS"]
      ds_data.each do |ds|
        hour = ds["Hour"]
        hour_length = hour.to_s.length
        hour = hour_length == 1 ? "0#{hour}" : "#{hour}"
        mon = ds["Mon"]
        mon = mon.length == 1 ? "0#{mon}" : mon
        day = ds["Day"]
        day = day.length == 1 ? "0#{day}" : day
        day_hour_time_str = "#{ds["Year"]}-#{mon}-#{day} #{hour}:00:00"
        day_hour_time = Time.parse(day_hour_time_str) + 60*60*8
        day_time = day_hour_time.strftime("%Y-%m-%d %H:00:00")
        win_d_inst_max = ds["WIN_D_INST_Max"] # 极大风速的风向(角度)
        win_s_inst_max = ds["WIN_S_Inst_Max"] # 极大风速(米/秒)
        rhu = ds["RHU"] # 相对湿度(%)
        tem = ds["TEM"] # 温度/气温	摄氏度(℃)
        pre_1h = ds["PRE_1h"] # 降雨(mm)
        station__flag = @w_station_id_arr.include?(ds["Station_Id_C"].to_i)
        begin
          DWetherDatum.create(:station_id => ds["Station_Id_C"].to_i,
                              :real_time => day_time,
                              :win_d_inst_max => win_d_inst_max,
                              :win_s_inst_max => win_s_inst_max,
                              :rhu => rhu,
                              :tem => tem,
                              :pre_1h => pre_1h)
          # rescue Exception => e
          #    ...
        end
      end
    else
      #  无数据或者出错产生的记录
      # ...
      File.open("#{Rails.root}/log/wetherFalse.log", "a+") do |yw|
        yw.syswrite(json)
      end
      print "False ≈≈≈≈", "\n"
    end
  end


end