module ApplicationHelper
  def set_cookie( key, value )
   cookies[key] = value
  end
  def am_system_user_name
    SRegionCode.find_by( "is_leaf = 0" ).region_name
  end
  
  ### 分页用的统一实例变量: @curr_page|@total_page|@size_pre_page
  def paging_magic( data_ar, current_page, page_size )
    if data_ar.nil?
      return
    end
    
    if page_size.nil?
      page_size   = 10
      @page_size  = 10
    end
    
    if current_page.nil?
      current_page  = 1
      @current_page = 1
    end
    @total_page = data_ar.length / page_size.to_i
    @total_page = @total_page + 1 if ((data_ar.length % page_size.to_i) > 0)
    
    start_rec = (current_page.to_i - 1) * page_size.to_i
    start_rec = 0 if start_rec < 0 
    
    if data_ar.class.to_s == "Array"
      data_ar[start_rec, page_size.to_i]
    else
      data_ar.offset( start_rec ).limit( page_size.to_i )
    end
  end
  
end
