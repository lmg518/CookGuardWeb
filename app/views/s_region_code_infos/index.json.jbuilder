json.region_code_info @region_code_info.each do |region_info|
    json.id region_info.id
    json.linkman region_info.linkman #负责人 
    json.linkman_tel region_info.linkman_tel #负责人电话 
    json.unit_address region_info.unit_address #单位所在地 
    json.unit_name region_info.unit_name #单位名称 
    json.unit_code region_info.unit_code #单位编号 
    json.status region_info.status #状态 
    json.station_id region_info.station_id #站点id
    json.s_region_code SRegionCode.select(:id,:region_name).find_by(:id =>region_info.s_region_code_id) #区域
end
json.s_region_code @d_region_code #一级区域
json.list_size @region_code_info.present? ? @region_code_info.total_count : 0 