json.users @d_login_msgs.each do |user|
    role = user.s_role_msg
    json.id user.id
    json.login_no user.login_no
    json.login_name user.login_name
    json.role_name role
    json.password user.password
    json.password_confirmation user.password_confirmation
    json.role_id user.s_role_msg_id
    json.valid_flag user.valid_flag
    json.valid_begin user.valid_begin
    json.valid_end user.valid_begin
    json.ip_address user.ip_address
    json.describe user.describe
    json.created_at user.created_at
    
end
json.page @page
json.count @count

json.allRole @roles.each do |role|
    json.id role.id
    json.role_name role.role_name
    json.valid_flag role.valid_flag
    json.root_distance role.root_distance
    json.queue_index role.queue_index
    json.created_at role.created_at
    json.updated_at role.updated_at
end
json.d_region_code @d_region_code
json.s_region_code_info @s_region_code_info