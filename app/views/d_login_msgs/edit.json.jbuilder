json.user {
    json.id @d_login_msg.id
    json.login_no @d_login_msg.login_no
    json.login_name @d_login_msg.login_name
    json.role_id @d_login_msg.s_role_msg_id
    json.password @d_login_msg.password
    json.password_confirmation @d_login_msg.password_confirmation
    json.valid_flag @d_login_msg.valid_flag
    json.valid_begin @d_login_msg.valid_begin
    json.valid_end @d_login_msg.valid_begin
    json.ip_address @d_login_msg.ip_address
    json.describe @d_login_msg.describe
    json.created_at @d_login_msg.created_at
    json.region_code @s_region_code.present? ? @s_region_code : ''
    json.father_region_code @father_region_code.present? ? @father_region_code : ''
    json.code_info @code_info.present? ? @code_info : ''
}