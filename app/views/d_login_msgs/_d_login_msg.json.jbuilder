# json.extract! d_login_msg, :id, :login_no, :login_name, :password, :password_confirmation, :password_digest, :valid_flag, :valid_begin, :valid_end, :ip_address, :describe, :created_at, :updated_at
# json.url d_login_msg_url(d_login_msg, format: :json)
json.d_login_msgs @d_login_msgs.each do |d_login_msg|
    json.id d_login_msg.id
end