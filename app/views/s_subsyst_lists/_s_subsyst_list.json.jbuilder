json.extract! s_subsys_list, :id, :sys_name, :sys_url, :mark, :created_at, :updated_at
json.url s_subsys_list_url(s_subsys_list, format: :json)
