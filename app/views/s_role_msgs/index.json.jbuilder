i = 0

json.roles @s_role_msgs.each do |role|
    json.id role.id
    json.sSubsystList @arr[i]
    json.role_name role.role_name
    json.valid_flag role.valid_flag
    json.root_distance role.root_distance
    json.queue_index role.queue_index
    json.created_at role.created_at
    json.updated_at role.updated_at
    i = i+1
end
json.page @page
json.count @count
