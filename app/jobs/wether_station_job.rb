include WetherStationHelper
class WetherStationJob < ActiveJob::Base

  def perform(*args)
    WetherStationHelper.init
  end

end