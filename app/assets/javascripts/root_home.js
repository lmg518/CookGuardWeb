

$(document).ready(function(){
    // 轮播图
    var bannerLi = $('.banner .container .bannerCarousel .bannerStart');

    var bannerPrev = $(".banner .container .bannerPrev .prev");

    var bannerNext = $(".banner .container .bannerNext .next");

    var bannerIndex = 0;

    var timer,timers;

    var opa = 0;
  
    //先让图片动起来
    function  show(a){

    	bannerIndex = a;

    	if(bannerIndex < 0) bannerIndex = bannerLi.length - 1;

    	if(bannerIndex == bannerLi.length) bannerIndex = 0;
    	
    	for(var i=0;i<bannerLi.length;i++){

    		bannerLi[i].style = 'opacity:0';

    	}
    	
    	clearInterval(timer);
    	timer = setInterval(function(){

    		opa += 2;

    		bannerLi[bannerIndex].style.opacity = opa / 100;

    		if(opa == 100){

    			opa = 0;

    			clearInterval(timer);

    		}
    	},20)
//      兼容思维
    //  bannerLi[bannerIndex].style.transition = '1s'
    //  bannerLi[bannerIndex].style.opacity = '1'
    }
    function autoPlay(){

    	timers = setInterval(function(){

    		bannerIndex++;
    		show(bannerIndex);

    	},3000)
    }

    // autoPlay(); 自主轮播

    bannerPrev.click(function(){

    	bannerIndex--;

    	show(bannerIndex);

    });
    bannerNext.click(function(){

    	bannerIndex++;

    	show(bannerIndex);

    });

    /* 
        add_yangna <2018-02-02 10:00>
        描述：首页banner渲染数据
    */

    function originList() {
         $.ajax({
                type:'get',
                url:'/user_subsyst_list.json',
                dataType:'json',
                async:true,
                beforeSend:function(){
                    $('.loading').show();
                },
                success:function(data){
                    console.log(data);
                    var listBanner = template("listBanner",data);
                    $("#headIndex .banner.col-lg-12 .bannerCarousel .bannerStart").html(listBanner);
                    $('.loading').hide();
                },
                complete:function(){
                    $('.loading').hide();
                }
         })
    }
    originList();


})
