$(function(){
	 $(".loading").show();
	$(document).ready(function(){
		var height = document.body.clientHeight;
        console.log("高"+height)
        var scrollHeight = height - 200;
        console.log(scrollHeight);
        $("#s_region_code_info .container .newsRight .listBanner").css("max-height",scrollHeight);
	
//	单位名称列表
	$.getJSON('/s_region_code_infos/get_ids').done(function(data){
		var region_result = template("region_select",data);
        $("#s_region_code_info select[name='region_list']").html(region_result);
	})
	/*查询*/
        $("#s_region_code_info .public_search .submit").click(function(){
        	var region_id=$("#s_region_code_info .public_search input[name='region_id']").val();
        	$.ajax({
                type:"get",
                url:"s_region_code_infos.json",
                dataType:"json",
                async:true,
                data:{
                    pageNow:1,
                    unit_name:region_id
                },
                beforeSend:function(){
                    $('.loading').hide();
                },
                success:function(data){
                    console.log(data);
                   // var result = template("region_table",data);
                   // $("#s_region_code_info .container .newsRight .listBanner").html(result);
                    var count = data.count;
                    $("#page").initPage(count, '1', originSearch);
                },
                 complete:function(){
                    $('.loading').hide();
                },
                error:function(err){
                    console.log(err);
                    alert("系统异常,请联系管理员!");
                    $('.loading').hide();
                }
            })
        })
	/*新建验证*/
            $('#new_region_form').bootstrapValidator({
              message : 'This value is not valid',
              fields : {
                linkman : {
                  message : '负责人',
                  validators : {
                    notEmpty : {
                      message : '负责人不能为空'
                    }
                  }
                },
                linkman_tel : {
                  message : '负责人电话',
                  validators : {
                    notEmpty : {
                      message : '负责人电话不能为空'
                    }
                  }
                },
                unit_address : {
                  message : '单位所在地',
                  validators : {
                    notEmpty : {
                      message : '单位所在地不能为空'
                    }
                  }
                },
                unit_name : {
                  message : '单位名称',
                  validators : {
                    notEmpty : {
                      message : '单位名称不能为空'
                    }
                  }
                },
                unit_code : {
                  message : '单位编号',
                  validators : {
                    notEmpty : {
                      message : '单位编号不能为空'
                    }
                  }
                }
                
              }
            });
            
            /*编辑验证*/
            $('#edit_region_form').bootstrapValidator({
              message : 'This value is not valid',
              fields : {
                linkman : {
                  message : '负责人',
                  validators : {
                    notEmpty : {
                      message : '负责人不能为空'
                    }
                  }
                },
                linkman_tel : {
                  message : '负责人电话',
                  validators : {
                    notEmpty : {
                      message : '负责人电话不能为空'
                    }
                  }
                },
                unit_address : {
                  message : '单位所在地',
                  validators : {
                    notEmpty : {
                      message : '单位所在地不能为空'
                    }
                  }
                },
                unit_name : {
                  message : '单位名称',
                  validators : {
                    notEmpty : {
                      message : '单位名称不能为空'
                    }
                  }
                },
                unit_code : {
                  message : '单位编号',
                  validators : {
                    notEmpty : {
                      message : '单位编号不能为空'
                    }
                  }
                }
                
              }
            });
            
        /*添加提交*/
        $('#new_region_form .register').click(function() {
        	var addValidator = $('#new_region_form').data('bootstrapValidator');
              addValidator.validate();
            var linkman= $("#new_region_form input[name='linkman']").val();
            var linkman_tel= $("#new_region_form input[name='linkman_tel']").val();
            var unit_address= $("#new_region_form input[name='unit_address']").val();
            var unit_name= $("#new_region_form input[name='unit_name']").val();
            var unit_code= $("#new_region_form input[name='unit_code']").val();
            var status = $("#new_region_form select[name='status'] option:selected").val();
            var group_id;
            /*判断区域最后一个选中的ID*/
            if($("#new_region_form .selectRegion_form .selectRegion:nth-last-of-type(1) option").length > 0) {
                group_id = $("#new_region_form .selectRegion_form .selectRegion:nth-last-of-type(1) option:selected").val();
                
            }else if($("#new_region_form .selectRegion_form .selectRegion:nth-last-of-type(2) option").length > 0) {
                group_id = $("#new_region_form .selectRegion_form .selectRegion:nth-last-of-type(2) option:selected").val();

            }else if($("#new_region_form .selectRegion_form .selectRegion:nth-of-type(1) option").length > 0) {
                group_id = $("#new_region_form .selectRegion_form .selectRegion:nth-of-type(1) option:selected").val();
            }
            console.log(group_id)
            if (addValidator.isValid()){
            	if(group_id == ""){
                        alert("请选择区域");
                    }else{
                    	 $.ajax({
                            url : '/s_region_code_infos.json',
                            type : 'post',
                            async:true,
                            dataType : "json",
                            data :{
                                region_code_info :{
                                	linkman:linkman,
                                	linkman_tel:linkman_tel,
                                	unit_address:unit_address,
                                	unit_name:unit_name,
                                	unit_code:unit_code,
                                	status:status,
                                	s_region_code_id:group_id
                                }
                            },
                            beforeSend:function(){
                                $('.loading').hide();
                            },
                            success : function(data) {
                                console.log(data);
                                    alert(data.status);
                                    $("#region_add").modal("hide");
                                    $('#new_region_form').data('bootstrapValidator').resetForm(true);
                                    originPage();
                               
                            },
                            complete:function(){
                                $('.loading').hide();
                            },
                            error : function(err) {
                                alert("系统异常,请联系管理员!");
                                console.log(err);
                                $('.loading').hide();
                            }
                        })
                    }
            }
        })
         /*点击编辑*/
         var editID;
         $("body").delegate("#s_region_code_info .container .newsRight .listBanner span.edit","click",function(){
         	editID = $(this).attr("value");
         	 $.ajax({
                  url : "/s_region_code_infos/"+editID+"/edit.json",
                  type : "get",
                  dataType : "json",
                  async:true,
                  beforeSend:function(){
                    $('.loading').hide();
                  },
                  success : function(data) {
                        var Data =  data.region_code_info;
                        var linkman=Data.linkman;
                        var linkman_tel=Data.linkman_tel;
                        var unit_address=Data.unit_address;
                        var unit_name=Data.unit_name;
                        var unit_code=Data.unit_code;
                        var status=Data.status;
                        var father_region_code = Data.father_region_code.id;
                        var region_code = Data.s_region_code.id;
                        var region_name = Data.s_region_code.region_name;
                        $("#edit_region_form input[name='linkman']").val(linkman);
                        $("#edit_region_form input[name='linkman_tel']").val(linkman_tel);
                        $("#edit_region_form input[name='unit_address']").val(unit_address);
                        $("#edit_region_form input[name='unit_name']").val(unit_name);
                        $("#edit_region_form input[name='unit_code']").val(unit_code);
                        $('#edit_region_form select[name="status"]').find('option[value="'+status+'"]').attr('selected',true);
                        $('#edit_region_form .fatherFir_region').find('option[value="'+father_region_code+'"]').attr('selected',true);
                        $('#edit_region_form .selectRegion_form .selectRegion').eq(1).find("select").html('<option value="'+region_code+'">'+region_name+'</option>');
                   },
                   complete:function(){
                        $('.loading').hide();
                  },
                  error : function(err) {
                      alert("系统异常,请联系管理员!");
                      console.log(err);
                      $('.loading').hide();
                  }
              })
         })
        /*编辑提交*/
        $("#edit_region_form .register").click(function(){
        	var editValidator = $('#edit_region_form').data('bootstrapValidator');
			editValidator.validate();
            var linkman= $("#edit_region_form input[name='linkman']").val();
            var linkman_tel= $("#edit_region_form input[name='linkman_tel']").val();
            var unit_address= $("#edit_region_form input[name='unit_address']").val();
            var unit_name= $("#edit_region_form input[name='unit_name']").val();
            var unit_code= $("#edit_region_form input[name='unit_code']").val();
            var status = $("#edit_region_form select[name='status'] option:selected").val();
            var group_id;
            /*判断区域最后一个选中的ID*/
            if($("#edit_region_form .selectRegion_form .selectRegion:nth-last-of-type(1) option").length > 0) {
                group_id = $("#edit_region_form .selectRegion_form .selectRegion:nth-last-of-type(1) option:selected").val();
                
            }else if($("#edit_region_form .selectRegion_form .selectRegion:nth-last-of-type(2) option").length > 0) {
                group_id = $("#edit_region_form .selectRegion_form .selectRegion:nth-last-of-type(2) option:selected").val();

            }else if($("#edit_region_form .selectRegion_form .selectRegion:nth-of-type(1) option").length > 0) {
                group_id = $("#edit_region_form .selectRegion_form .selectRegion:nth-of-type(1) option:selected").val();
            }
            if(editValidator.isValid()){
            	if(group_id == ""){
                        alert("请选择区域");
                    }else{
                    	$.ajax({
			                type:"put",
			                dataType:"json",
			                url:"s_region_code_infos/"+editID+".json",
			                async:true,
			                data :{
			                    region_code_info :{
			                    	linkman:linkman,
			                    	linkman_tel:linkman_tel,
			                    	unit_address:unit_address,
			                    	unit_name:unit_name,
			                    	unit_code:unit_code,
			                    	status:status,
			                    	s_region_code_id:group_id
			                    }
			                },                
			                beforeSend:function(){
			                    $('.loading').show();
			                },
			                success:function(data){
			                   
			                        alert("修改成功")
			                        $("#region_edit").modal("hide");
			                        originPage();
			                   
			                },
			                complete:function(){
			                    $('.loading').hide();
			                },
			                error:function(err){
			                     alert("系统异常,请联系管理员!");
			                     $('.loading').hide();
			                }
			            })
                    }
            }
            
        })
        
        function originPage(page){
        	$.ajax({
                type:'get',
                url:'/s_region_code_infos.json',
                dataType:'json',
                async:true,
                data:{
                    pageNow:page,
                },
                beforeSend:function(){
                    $('.loading').show();
                },
                success:function(data){
                    console.log(data);
                    var result = template("region_table",data);
//                 
                    $("#s_region_code_info .container .newsRight .listBanner").html(result);
//                  
                    var group_region = template("group_region",data); //区域
                    
                    $("#new_region_form select[name='group_id']").eq(0).html(group_region);  //添加--区域
                    $("#edit_region_form select[name='group_id']").eq(0).html(group_region); //编辑--区域
//                 
                    $("body").delegate("#s_region_code_info select.fatherFir_region","change",function(){
                        var father_region_id = $(this).val();
                        console.log(father_region_id);
                         $.ajax({
                            type:'get',
                            url:'d_login_msgs/region_codes',
                            dataType:'json',
                            async:true,
                            data:{
                                father_region_id:father_region_id,
                            },
                            beforeSend:function(){
                                $('.loading').show();
                            },
                            success:function(data){
                                console.log(data);
                                $('.loading').hide();
                                var group_regionSec = template("group_regionSec",data);
                                $("#new_region_form select[name='group_id']").eq(1).html(group_regionSec); //添加--二级区域
                                $("#edit_region_form select[name='group_id']").eq(1).html(group_regionSec);//编辑--二级区域
                            },
                            error:function(err){
                                console.log(err);
                                $('.loading').hide();
                                
                            }
                         })
                    })
                },
                complete:function(){
                    $('.loading').hide();
                },
                error:function(err){
                     alert("系统异常,请联系管理员!");
                     $(".loading").hide();
                }
            });
           }
           $.ajax({
                type:'get',
                url:'/s_region_code_infos.json',
                dataType:'json',
                async:true,
                beforeSend:function(){
                    $('.loading').show();
                },
                success:function(data){
                    console.log(data);
                    var count = data.list_size;
                    $(".yn_public_page .pageTotal a").text(count);
                    console.log(count)
                    var result = template("region_table",data);
//                 
                    $("#s_region_code_info .container .newsRight .listBanner").html(result);
//                 
                    $("#page").initPage(count, '1', originPage);
                },
                complete:function(){
                    $('.loading').hide();
                },
                error:function(err){
                     alert("系统异常,请联系管理员!");
                     $(".loading").hide();
                }
            }); 
                /*查询*/
        function originSearch(page){
            var region_id=$("#s_region_code_info .public_search input[name='region_id']").val();
             $.ajax({
                type:'get',
                url:'/s_region_code_infos.json',
                dataType:'json',
                async:true,
                data:{
                    pageNow:page,
                    unit_name:region_id
                },
                beforeSend:function(){
                    $('.loading').show();
                },
                success:function(data){
                	var count = data.list_size;
                    $(".yn_public_page .pageTotal a").text(count);
                    console.log(data);
                    var result = template("region_table",data);
                    $("#s_region_code_info .container .newsRight .listBanner").html(result);
                    
                },
                complete:function(){
                    $('.loading').hide();
                },
                error:function(err){
                     alert("系统异常,请联系管理员!");
                     $(".loading").hide();
                }
            });
        }
           })
})