   $(function(){
       var session = $("#role_limit small.session.hide").text();
       console.log(session)
        var $height;
        var $scrollHeight;
        $(window).resize(function(){
            $height = $("#role_limit").height();
            console.log($height)
            $scrollHeight = $height - 200;
            console.log($scrollHeight);
            $("#role_limit .newsRight .listBanner").css("max-height",$scrollHeight);
        })
        $(document).ready(function(){
            $height = $("#role_limit").height();
            console.log($height)
            $scrollHeight = $height - 200;
            console.log($scrollHeight);
            $("#role_limit .newsRight .listBanner").css("max-height",$scrollHeight);
            /*平台菜单*/
            $.ajax({
                type:'get',
                url:'/user_subsyst_list.json',
                dataType:'json',
                async:true,
                beforeSend:function(){
                    $('.loading').show();
                },
                success:function(data){
                    console.log(data);
                    data.subsyst.map(function(ele,index,arr){
                        console.log(ele);
                        var list = "<div class='col-sm-4'><input type='checkbox' value="+ele.id+"><span>"+ele.sys_name+
                           "</span></div>";
                        var list_config = "<label class='col-xs-2 menuList control-label'><span valueSrc="+ele.sys_url+" value="+ele.id+">"+ele.sys_name+"</span></label>"
                        $("#limit_add #limit_add_form .list").append(list);
                        $("#limit_edit #limit_edit_form .list").append(list);
                        $("#role_limit #add_config .line-top").append(list_config);
                        $("#role_limit #add_config .line-top .menuList").eq(0).addClass("goodBack");
                    })
                },
                complete:function(){
                    $('.loading').hide();
                },
                error:function(err){
                    console.log(err);
                    alert("系统异常,请联系管理员!");
                    $('.loading').hide();
                }
            })

            /*权限切换JS实现*/
            $("body").delegate("#role_limit .list_config .che1 p span","click",function(){
                $(this).parent("p").next('.che2').slideToggle();
                $(this).parent("p").find("span").addClass("goodColor");
                $(this).parents(".che1").siblings(".che1").find("p span").removeClass("goodColor");
            });
             $("body").delegate("#role_limit .list_config .che1 p input[type='checkbox']","click",function(){
                console.log($(this));
                console.log($(this).parent("p").next().html());
                 $(this).parent("p").next().find('li input[type="checkbox"]').prop("checked",this.checked);
             })
           
            /*新建验证*/
            $('#limit_add_form').bootstrapValidator({
              message : 'This value is not valid',
              fields : {
                role_name : {
                  message : '角色名称',
                  validators : {
                    notEmpty : {
                      message : '角色名称不能为空'
                    }
                  }
                }
              }
            });
               /*编辑验证*/
            $('#limit_edit_form').bootstrapValidator({
              message : 'This value is not valid',
              fields : {
                role_name : {
                  message : '角色名称',
                  validators : {
                    notEmpty : {
                      message : '角色名称不能为空'
                    }
                  }
                }
              }
            });
            /*查询*/
            $("#role_limit .public_search .submit").click(function(){
                var role_name =  $("#role_limit .public_search input[name='role_name']").val();
                $.ajax({
                      url : "s_role_msgs.json",
                      type : "get",
                      dataType:"json",
                      async:true,
                      data:{
                        role_name:role_name
                      },
                    beforeSend:function(){
                        $(".loading").show();
                    },
                    success:function(data){
                        console.log(data.count);
                        var result = template("roleList",data);
                        $("#role_limit .newsRight .listBanner").html(result);
                        $("#page").initPage(data.count, '1', originSearch);
                    },
                    complete:function(){
                        $('.loading').hide();
                    },
                    error:function(err){
                        console.log(err);
                        alert("系统异常,请联系管理员!");
                        $('.loading').hide();
                    }
                })
            })

            /*新增提交*/
            $('#limit_add_form .submit').click(function(){
                var role_name = $("#limit_add_form input[name='role_name']").val();
                var valid_flag  = $('#limit_add_form input[name="valid_flag"]:checked').val();  //是否启用
                var addValidator = $('#limit_add_form').data('bootstrapValidator');
                var func_code_true=[];
                $('#limit_add #limit_add_form .list input[type="checkbox"]:checked').each(function(i){
                    func_code_true.push($('#limit_add #limit_add_form .list input[type="checkbox"]:checked').eq(i).val());
                })
                if(func_code_true.length <= 0){
                    func_code_true="";
                }
                 addValidator.validate();
                  if (addValidator.isValid()) {
                    $.ajax({
                        url : "s_role_msgs.json",
                        type : 'post',
                        async:true,
                        data :{
                            s_role_msg:{
                                role_name:role_name,
                                valid_flag:valid_flag
                            },
                            func_code_true:func_code_true
                        },
                        beforeSend:function(){
                            $(".loading").show();
                        },
                        success:function(data){
                            if(data.status=="新增成功") {
                                alert(data.status);
                                originPage();
                                $("#limit_add").modal("hide");
                            }else{
                                alert(data.status);
                            }
                        },
                         complete:function(){
                            $(".loading").hide();
                        },
                        error:function(err){
                            console.log(err);
                             alert("系统异常,请联系管理员!");
                             $(".loading").hide();
                        }
                    })
                  }
                  $('#limit_add_form').data('bootstrapValidator').resetForm(true);
            })
            /*编辑*/
             var editID;
            $("body").delegate("#role_limit .container .newsRight .listBanner .bottom .edit","click",function(){
                editID = $(this).attr("value");
                $("#limit_edit #limit_edit_form .list input[type='checkbox']").prop("checked",false);
                $.ajax({
                    type:"get",
                    url:"/s_role_msgs/"+editID+"/edit.json",
                    dataType:"json",
                    async:true,
                    beforeSend:function(){
                        $(".loading").show();
                    },
                    success:function(data){
                        console.log(data)
                        var role_name = data.role.role_name;
                        var valid_flag = data.role.valid_flag;
                        $("#limit_edit #limit_edit_form input[name='role_name']").val(role_name);
                        //$("#limit_edit #limit_edit_form .Radio").find('input[value="+valid_flag+"]').attr("checked","checked");
                        if (valid_flag == 1){
                            $("#limit_edit #limit_edit_form .Radio #optionsRadios1").prop('checked',true);
                        } else if (valid_flag == 0){
                            $("#limit_edit #limit_edit_form .Radio #optionsRadios2").prop('checked',true);
                        }
                        var checkedID=[];
                        var allID = [];
                        data.func_code_trues.map(function(ele,index,arr){
                            checkedID.push(ele.s_func_msg_id);
                           // $("#limit_edit #limit_edit_form .list").find('input[value="+ele.s_func_msg_id+"]').prop("checked",true);
                        })
                        $("#limit_edit #limit_edit_form .list input[type='checkbox']").each(function(i){
                             allID.push($("#limit_edit #limit_edit_form .list input[type='checkbox']").eq(i).val());
                        })
                       
                        unChecked=allID.map(function(key){
                            return Number(key)
                        });
                        console.log(checkedID);
                        console.log(unChecked);
                        checkedID.map(function(ele,index,arr){
                            var check=unChecked.indexOf(ele);
						    console.log(check);
                            //$("#limit_edit #limit_edit_form .list input[type='checkbox']").prop("checked",false);
                            $("#limit_edit #limit_edit_form .list .col-sm-4").eq(check).find("input[type='checkbox']").prop("checked",true);
                            console.log($("#limit_edit #limit_edit_form .list").html())
                        });

                    },
                    complete:function(){
                        $(".loading").hide();
                    },
                    error:function(err){
                        console.log(err);
                         alert("系统异常,请联系管理员!");
                         $(".loading").hide();
                    }
                })
            })
            /*编辑提交*/
            $('#limit_edit_form .submit').click(function(){
                var role_name = $("#limit_edit_form input[name='role_name']").val();
                var valid_flag  = $('#limit_edit_form input[name="valid_flag"]:checked').val();  //是否启用
                var editValidator = $('#limit_edit_form').data('bootstrapValidator');
                var func_code_true=[];
                var func_code_falses = [];
                for(var i=0;i<$("#limit_edit #limit_edit_form .list input[type='checkbox']").length;i++ ){
                    if($("#limit_edit #limit_edit_form .list input[type='checkbox']")[i].checked == true){
                        func_code_true.push($("#limit_edit #limit_edit_form .list input[type='checkbox']")[i].value);
                    }else{
                        func_code_falses.push($("#limit_edit #limit_edit_form .list input[type='checkbox']")[i].value);
                    }
                 }

                console.log(func_code_true)
                console.log(func_code_falses)
                 editValidator.validate();
                  if (editValidator.isValid()) {
                    $.ajax({
                        url : "s_role_msgs/"+editID,
                        type : 'put',
                        async:true,
                        data :{
                            s_role_msg:{
                                role_name:role_name,
                                valid_flag:valid_flag
                            },
                            func_code_true:func_code_true,
                            func_code_falses:func_code_falses
                        },
                        beforeSend:function(){
                            $(".loading").show();
                        },
                        success:function(data){
                            console.log(data);
                            $("#limit_edit").modal("hide");
                            originPage();
                        },
                        complete:function(){
                            $(".loading").hide();
                        },
                        error:function(err){
                            console.log(err);
                             alert("系统异常,请联系管理员!");
                             $(".loading").hide();
                        }
                    })
                  }
            });
          
            /*配置*/
  /*======= ===========================================================================================
            var configID;
            var roleName;
            var status;
            var configSrc;
             $("body").delegate("#role_limit .container .newsRight .listBanner .bottom .config","click",function(){
                configID = $(this).attr("value");
                roleName = $(this).parent(".bottom").siblings(".top").find(".user").text();
                status = $(this).parent(".bottom").siblings(".top").find(".status").attr("value");
                var configValue = $("#add_config .modal-body .line-top label.menuList").eq(0).find("span").attr("value");
                configSrc = $("#add_config .modal-body .line-top label.menuList").eq(0).find("span").attr("valueSrc");
                console.log(configSrc);
                console.log(configValue);
                if(configValue == "1"){
                    alert("111")
                    configFunc(configSrc+"/s_role_msgs/"+configID+"/edit.json"); /空气站监测 /
                }else if(configValue == "2"){
                    configFunc(configSrc+"/system/s_func_msgs.json");/水质监测/
                    limitCheck(configSrc+"/system/s_role_msgs/"+configID+".json?session="+session);
                }else if(configValue == "3"){
                    configFunc(configSrc+"/s_func_msgs.json");  /运维管理/
                    limitCheck(configSrc+"/s_role_msgs/"+configID+".json?session="+session);
                }else if(configValue == "4"){
                    configFunc(configSrc+"/s_func_msgs.json");   /污染源监控/
                    limitCheck(configSrc+"/s_role_msgs/"+configID+".json?session="+session);
                }
                $("#add_config .modal-body .line-top label.menuList").removeClass("goodBack");
                $("#add_config .modal-body .line-top label.menuList").eq(0).addClass("goodBack");
             });
            $("body").delegate("#role_limit #add_config #add_config_form .line-top .menuList","click",function(){
                $(this).addClass("goodBack").siblings(".menuList").removeClass("goodBack");
                var configValue = $(this).find("span").attr("value");
                configSrc = $(this).find("span").attr("valuesrc");
                if(configValue == "1"){
                    configFunc(configSrc+"/s_role_msgs/"+configID+"/edit.json?session="+session);
                }else if(configValue == "2"){
                    configFunc(configSrc+"/system/s_func_msgs.json?session="+session);
                    limitCheck(configSrc+"/system/s_role_msgs/"+configID+".json?session="+session);

                }else if(configValue == "3"){
                    configFunc(configSrc+"/s_func_msgs.json");
                    limitCheck(configSrc+"/s_role_msgs/"+configID+".json?session="+session);
                }else if(configValue == "4"){
                    configFunc(configSrc+"/s_func_msgs.json");
                    limitCheck(configSrc+"/s_role_msgs/"+configID+".json?session="+session);
                }
            })
             /配置提交/
            $("#add_config #add_config_form .submit").click(function(){
                var func_code_true=[];
                var func_code_false = [];
                for(var i=0;i<$("#add_config .list_config input[type='checkbox']").length;i++ ){
                    if($("#add_config .list_config input[type='checkbox']")[i].checked == true){
                        func_code_true.push($("#add_config .list_config input[type='checkbox']")[i].value);
                    }else{
                        func_code_false.push($("#add_config .list_config input[type='checkbox']")[i].value);
                    }
                 }
                 $(".list_config").find("input[name='func_code_true']").val(func_code_true);
                 $(".list_config").find("input[name='func_code_false']").val(func_code_false);
                 if($("#add_config .modal-body .line-top label.goodBack span").attr("value")=="1"){
                    var airData = {
                        s_role_msg:{
                            role_name:roleName,
                            valid_flag:status
                        },
                        session:session,
                        func_code_true: $(".list_config input[name='func_code_true']").val(),
                        func_code_false:$(".list_config input[name='func_code_false']").val(),
                    };
                    configLimit_submit(configSrc+"/s_role_msgs/"+configID+".json",airData);
                 }else if($("#add_config .modal-body .line-top label.goodBack span").attr("value")=="2"){
                    var waterData = {
                        role_name:roleName,
                        valid_flag:status,
                        func_codes:func_code_true
                    };
                    configLimit_submit(configSrc+"/system/s_role_msgs/"+configID+".json?session="+session,waterData);
                 }else if($("#add_config .modal-body .line-top label.goodBack span").attr("value")=="3"){
                    var opareData = {
                        s_role_msg:{
                            role_name:roleName,
                            valid_flag:status
                        },
                        session:session,
                        func_code_true: func_code_true,
                        func_code_false:func_code_false
                    };
                    configLimit_submit(configSrc+"/s_role_msgs/"+configID+".json?session="+session,opareData);
                 }else if($("#add_config .modal-body .line-top label.goodBack span").attr("value")=="4"){
                    var polluteData = {
                        role_name:roleName,
                        valid_flag:status,
                        func_codes:func_code_true
                    };
                    configLimit_submit(configSrc+"/s_role_msgs/"+configID+"?session="+session,polluteData);
                 }
            })
            /权限配置数据展示--空气站选中/
            function configFunc(url){
                $.ajax({
                     type:'get',
                     url:url,
                     dataType:'json',
                     async:true,
                     data:{
                        session:session
                     },
                    beforeSend:function(){
                        $(".loading").show();
                    },
                    success:function(data){
                        var all = [];
                        var allID = [];
                        var checkedID = []; 
                        console.log(data);
                        if($("#add_config .modal-body .line-top label.goodBack span").attr("value")=="1"){
                             var air_result = template("list_air",data);
                             $("#add_config .list_config").html(air_result);
                        }else if($("#add_config .modal-body .line-top label.goodBack span").attr("value")=="2"){
                             var water_result = template("list_water",data);
                             $("#add_config .list_config").html(water_result);
                        }else if($("#add_config .modal-body .line-top label.goodBack span").attr("value")=="3"){
                             var water_result = template("list_water",data);
                             $("#add_config .list_config").html(water_result);
                        }else if($("#add_config .modal-body .line-top label.goodBack span").attr("value")=="4"){
                             var water_result = template("list_water",data);
                             $("#add_config .list_config").html(water_result);
                        }
                        var a = $("#role_limit #add_config .list_config li input");
                        $("#role_limit #add_config .list_config li input").each(function(i){
                            console.log($("#role_limit #add_config .list_config").html());
                            all.push($("#add_config .list_config li input").eq(i).val());
                        });
                        allID=all.map(function(key){
                            return Number(key)
                        });
                        if(data.user_role_funs){
                            var user_role_funs = data.user_role_funs;
                            user_role_funs.map(function(ele,index,arr){
                                console.log(ele)
                                checkedID.push(ele.s_func_msg_id);
                             });
                        }
                       
                        checkedID.map(function(ele,index,arr){
                            var check=allID.indexOf(ele);
                            $("#role_limit .list_config  input[type='checkbox").eq(check).prop("checked",true);
                        });
                        console.log(allID);
                        console.log(checkedID);
                    },
                    complete:function(){
                        $(".loading").hide();
                    },
                    error:function(err){
                        console.log(err);
                        alert("系统异常,请联系管理员!");
                        $('.loading').hide();

                    }
                 })
            }
        /权限选中/
        function limitCheck(URL){
           $.ajax({
                type:"get",
                url:URL,
                success:function(data){
                        console.log(data);
                        var waterAll = [];
                        var water_allID = [];
                        var water_checkedID = []; 
                        var waterFuns_chil = [];
                        $("#role_limit #add_config .list_config li input").each(function(i){
                            console.log($("#role_limit #add_config .list_config").html());
                            waterAll.push($("#add_config .list_config li input").eq(i).val());
                        });
                        water_allID=waterAll.map(function(key){
                            return Number(key)
                        });
                        var waterFuns = data.funcs;
                        waterFuns.map(function(ele,index,arr){
                            water_checkedID.push(ele.id);
                            ele.chil.map(function(element,ind,array){
                                waterFuns_chil.push(element.id);
                            })
                        });
                        water_checkedID = water_checkedID.concat(waterFuns_chil);
                        water_checkedID.map(function(ele,index,arr){
                            var check=water_allID.indexOf(ele);
                            console.log(check);
                            $("#role_limit #add_config .list_config input[type='checkbox']").eq(check).prop("checked",true);
                        });
                },
                error:function(err){
                    console.log(err)
                }
            })
          }
            /配置提交function/
            function configLimit_submit(URL,Data){
                $.ajax({
                    type:"put",
                    dataType:"json",
                    async:false,
                    url:URL,
                    data:Data,
                    beforeSend:function(xhr){
                        console.log(xhr);
                        $(".loading").show();
                    },
                    success:function(data){
                        console.log(data);
                        alert(data.msg);
                        $(".loading").hide();
                        $("#add_config").modal("hide");
                    },
                    complete:function(){
                        $(".loading").hide();
                    },
                    error:function(err){
                        alert("系统异常,请联系管理员!");
                        $('.loading').hide();
                    }
                 })
             } 
             ==================================================================================*/
             /*初始数据*/
            function originPage(page){
                var role_name = $(".public_search input[name='role_name']").val();
                $.ajax({
                    type:'get',
                    url:'s_role_msgs.json',
                    dataType:'json',
                    async:true,
                    data:{
                        pageNow:page,
                        
                    },
                    beforeSend:function(){
                        $(".loading").show();
                    },
                    success:function(data){
                        console.log(data);
                        var result = template("roleList",data);
                        $("#role_limit .newsRight .listBanner").html(result);
                        var roles = data.roles;
                        roles.map(function(ele,index,arr){
                            var sSubsystList = ele.sSubsystList;
                            sSubsystList.map(function(element,i,array){
                                console.log(element)
                            })
                        })
                    },
                    complete:function(){
                        $(".loading").hide();
                    },
                    error:function(err){
                        console.log(err);
                        alert("系统异常,请联系管理员!");
                        $('.loading').hide();
                    }
                });
            }
            $.ajax({
                  type:"get",
                  url:'s_role_msgs.json',
                  dataType:'json',
                  async:true,
                  beforeSend:function(){
                        $(".loading").show();
                  },
                   success:function(data){
                        console.log(data);
                        var result = template("roleList",data);
                        $("#role_limit .newsRight .listBanner").html(result);
                        var count = data.count;
                        $(".yn_public_page .pageTotal a").text(count);
                        console.log(count)
                        $("#role_limit .newsRight .listBanner").html(result);
                        $("#page").initPage(count, '1', originPage);
                    },
                    complete:function(){
                        $(".loading").hide();
                    },
                    error:function(err){
                        console.log(err);
                        alert("系统异常,请联系管理员!");
                        $('.loading').hide();
                    }

            })
            originPage();
                /*查询*/
        function originSearch(page){
            var role_name = $(".public_search input[name='role_name']").val();
             $.ajax({
                type:'get',
                url:'/s_role_msgs.json',
                dataType:'json',
                async:true,
                data:{
                    pageNow:page,
                    role_name:role_name
                },
                beforeSend:function(){
                    $('.loading').show();
                },
                success:function(data){
                	var count = data.count;
                        $(".yn_public_page .pageTotal a").text(count);
                    console.log(data);
                    var result = template("roleList",data);
                        $("#role_limit .newsRight .listBanner").html(result);
                    
                },
                complete:function(){
                    $('.loading').hide();
                },
                error:function(err){
                     alert("系统异常,请联系管理员!");
                     $(".loading").hide();
                }
            });
        }

        })
    })