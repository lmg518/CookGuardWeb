    $(function(){
        $(".loading").show();
        var $height;
        var $scrollHeight;
        $(window).resize(function(){
            $height = $("#d_login_msg").height();
            console.log($height)
            $scrollHeight = $height - 200;
            console.log($scrollHeight);
            $("#d_login_msg .container .newsRight .listBanner").css("max-height",$scrollHeight);
        })
        $(document).ready(function(){
            $height = $("#d_login_msg").height();
            console.log($height)
            $scrollHeight = $height - 200;
            console.log($scrollHeight);
            $("#d_login_msg .container .newsRight .listBanner").css("max-height",$scrollHeight);
            var time = new Date();
            var year = time.getFullYear();
            var month = time.getMonth() + 1;
            var day = time.getDate();
            var hour = time.getHours();
            var minnute = time.getMinutes();
            var second = time.getSeconds();
            var result = year + '-' + (month < 10 ? '0' + month : month) + '-'
			+ (day < 10 ? '0' + day : day);
            var resultTime = year + '-' + (month < 10 ? '0' + month : month) + '-'
			+ (day < 10 ? '0' + day : day) + ' ' + (hour < 10 ? '0' + hour : hour) +':'
            + (minnute < 10 ? '0' + minnute : minnute)+':'+(second < 10 ? '0' + second : second);
	        $('.datetimepicker').val(result);
	        $(".modal input[name='created_at']").val(resultTime);
            $(".modal input[name='update_at']").val(resultTime);
            $('.datetimepicker').datetimepicker({
                format : "Y-m-d",
                minDate : year + '-01-01',
                maxDate : year + '-12-31',
                todayButton : true,
                timepicker : false
            });
            /*新建验证*/
            $('#new_login_form').bootstrapValidator({
              message : 'This value is not valid',
              fields : {
                login_no : {
                  message : '工号编码',
                  validators : {
                    notEmpty : {
                      message : '工号编码不能为空'
                    }
                  }
                },
                login_name : {
                  message : '工号名称',
                  validators : {
                    notEmpty : {
                      message : '工号名称不能为空'
                    }
                  }
                },
                password : {
                  validators : {
                    notEmpty : {
                      message : '密码不能为空'
                    },
                    identical : {
                      field : 'passwordConfirmation',
                      message : '两次输入密码不一致'
                    }
                  }
                },
                password_confirmation : {
                  validators : {
                    notEmpty : {
                      message : '验证密码不能为空'
                    },
                    identical : {
                      field : 'password',
                      message : '两次输入密码不一致'
                    }
                  }
                }
              }
            });
            /*编辑验证*/
            $('#edit_login_form').bootstrapValidator({
              message : 'This value is not valid',
              fields : {
                login_no : {
                  message : '工号编码',
                  validators : {
                    notEmpty : {
                      message : '工号编码不能为空'
                    }
                  }
                },
                login_name : {
                  message : '工号名称',
                  validators : {
                    notEmpty : {
                      message : '工号名称不能为空'
                    }
                  }
                },
                password : {
                  validators : {
                    notEmpty : {
                      message : '密码不能为空'
                    },
                    identical : {
                      field : 'passwordConfirmation',
                      message : '两次输入密码不一致'
                    }
                  }
                },
                password_confirmation : {
                  validators : {
                    notEmpty : {
                      message : '验证密码不能为空'
                    },
                    identical : {
                      field : 'password',
                      message : '两次输入密码不一致'
                    }
                  }
                }
              }
            });
            /**添加区域添加到input框 */
            $("body").delegate("#d_login_msg #add_region_modal .modal-footer .btn-sure","click",function(){
                var $val = $("#d_login_msg #add_region_modal .modal-body .region_station input[type='radio']:checked").val();
                var $text = $("#d_login_msg #add_region_modal .modal-body .region_station input[type='radio']:checked").next().text();
                console.log($val);
                console.log($text);
                $("#d_login_msg #role_add .regionGroup_text").val($text);
                $("#d_login_msg #role_add .regionGroup_id").val($val);
                $("#d_login_msg #add_region_modal").modal("hide");
            });
             /**编辑区域添加到input框 */
            $("body").delegate("#d_login_msg #edit_region_modal .modal-footer .btn-sure","click",function(){
                alert("1");
                var $val = $("#d_login_msg #edit_region_modal .modal-body .region_station input[type='radio']:checked").val();
                var $text = $("#d_login_msg #edit_region_modal .modal-body .region_station input[type='radio']:checked").next().text();
                console.log($val);
                console.log($text);
                $("#d_login_msg #role_edit .regionGroup_text").val($text);
                $("#d_login_msg #role_edit .regionGroup_id").val($val);
                $("#d_login_msg #edit_region_modal").modal("hide");
            })
        /*查询*/
        $("#d_login_msg .public_search .submit").click(function(){
            var login_name = $("#d_login_msg .public_search input[name='login_name']").val();
            var role_id = $("#d_login_msg .public_search select[name='role_id'] option:selected").val();
           
            $.ajax({
                type:"get",
                url:"d_login_msgs.json",
                dataType:"json",
                async:true,
                data:{
                    pageNow:1,
                    login_name:login_name,
                    role_id:role_id
                },
                beforeSend:function(){
                    $('.loading').hide();
                },
                success:function(data){
                    console.log(data);
                    var result = template("role_table",data);
                    $("#d_login_msg .container .newsRight .listBanner").html(result);
                    var count = data.count;
                    $("#page").initPage(count, '1', originSearch);
                },
                 complete:function(){
                    $('.loading').hide();
                },
                error:function(err){
                    console.log(err);
                    alert("系统异常,请联系管理员!");
                    $('.loading').hide();
                }
            })
        });
        
          /*添加提交*/
          $('#new_login_form .register').click(function() {
              var addValidator = $('#new_login_form').data('bootstrapValidator');
              addValidator.validate();
                var login_no = $("#new_login_form input[name='login_no']").val();
                var login_name = $("#new_login_form input[name='login_name']").val();
                var password = $("#new_login_form input[name='password']").val();
                var password_confirmation = $("#new_login_form input[name='password_confirmation']").val();
                var valid_flag = $("#new_login_form select[name='valid_flag'] option:selected").val();
                var created_at = $("#new_login_form input[name='created_at']").val();
                var update_at = $("#new_login_form input[name='update_at']").val();
                var valid_begin = $("#new_login_form input[name='valid_begin']").val();
                var valid_end = $("#new_login_form input[name='valid_end']").val();
                var describe = $("#new_login_form input[name='describe']").val();
                var role_id = $("#new_login_form select[name='role_id'] option:selected").val();
                var s_region_code_info_id = $("#new_login_form select[name='s_region_code_info_id'] option:selected").val(); // 所属单位
                var group_id = $("#new_login_form input[name='group_id']").val();
                var group_name = $("#new_login_form input.regionGroup_text").val();
                /*判断区域最后一个选中的ID*/
                /*if($("#new_login_form .selectRegion_form .selectRegion:nth-last-of-type(1) option").length > 0) {
                    group_id = $("#new_login_form .selectRegion_form .selectRegion:nth-last-of-type(1) option:selected").val();
                    
                }else if($("#new_login_form .selectRegion_form .selectRegion:nth-last-of-type(2) option").length > 0) {
                    group_id = $("#new_login_form .selectRegion_form .selectRegion:nth-last-of-type(2) option:selected").val();

                }else if($("#new_login_form .selectRegion_form .selectRegion:nth-of-type(1) option").length > 0) {
                    group_id = $("#new_login_form .selectRegion_form .selectRegion:nth-of-type(1) option:selected").val();
                }*/
                console.log(group_id)
              if (addValidator.isValid()) {
                   if(group_id == "" || group_name == ""){
                        alert("请选择区域");
                    }else{
                        $.ajax({
                            url : '/d_login_msgs.json',
                            type : 'post',
                            async:true,
                            dataType : "json",
                            data :{
                                d_login_msg:{
                                login_no:login_no,
                                login_name:login_name,
                                password:password,
                                password_confirmation:password_confirmation,
                                valid_flag:valid_flag,
                                created_at:created_at,
                                update_at:update_at,
                                valid_begin:valid_begin,
                                valid_end:valid_end,
                                describe:describe,
                                s_role_msg_id:role_id,
                                group_id:group_id,
                                s_region_code_info_id:s_region_code_info_id
                                }
                            },
                            beforeSend:function(){
                                $('.loading').hide();
                            },
                            success : function(data) {
                                console.log(data);
                                if(data.status == "工号重复") {
                                    alert(data.status);
                                } else {
                                    alert(data.status);
                                    $("#role_add").modal("hide");
                                    $('#new_login_form').data('bootstrapValidator').resetForm(true);
                                    originPage();
                                }
                            },
                            complete:function(){
                                $('.loading').hide();
                            },
                            error : function(err) {
                                alert("系统异常,请联系管理员!");
                                console.log(err);
                                $('.loading').hide();
                            }
                        })
                    }
              }
              
          })
          /*点击编辑*/
         var editID;
         $("body").delegate("#d_login_msg .container .newsRight .listBanner span.edit","click",function(){
            editID = $(this).attr("value");
              $.ajax({
                  url : "/d_login_msgs/"+editID+"/edit.json",
                  type : "get",
                  dataType : "json",
                  async:true,
                  beforeSend:function(){
                    $('.loading').hide();
                  },
                  success : function(data) {
                        var Data =  data.user;
                        var login_no = Data.login_no;
                        var login_name = Data.login_name;
                        var valid_flag = Data.valid_flag;
                        var valid_begin = Data.valid_begin;
                        var valid_end = Data.valid_end;
                        var describe = Data.describe;
                        var ip_address = Data.ip_address;
                        var role_id = Data.role_id;
                        var father_region_code = Data.father_region_code.id;
                        var region_code = Data.region_code.id;
                        var region_name = Data.region_code.region_name;
                        var code_info = Data.code_info.id; //所属单位
                        console.log("role_id"+role_id);
                        $("#edit_login_form input[name='login_no']").val(login_no);
                        $("#edit_login_form input[name='login_name']").val(login_name);
                        $("#edit_login_form input[name='valid_begin']").val(valid_begin);
                        $("#edit_login_form input[name='valid_end']").val(valid_end);
                        $("#edit_login_form input[name='describe']").val(describe);
                        $("#edit_login_form input[name='ip_address']").val(ip_address);
                        $('#edit_login_form select[name="valid_flag"]').find('option[value="'+valid_flag+'"]').attr('selected',true);
                        $('#edit_login_form select[name="role_id"]').find('option[value="'+role_id+'"]').attr('selected',true);
                        $("#edit_login_form input.regionGroup_text").val(region_name);
                        $("#edit_login_form input.regionGroup_id").val(region_code);
                        /*$('#edit_login_form .fatherFir_region').find('option[value="'+father_region_code+'"]').attr('selected',true);
                        $('#edit_login_form .selectRegion_form .selectRegion').eq(1).find("select").html('<option value="'+region_code+'">'+region_name+'</option>');
                        $('#edit_login_form .selectRegion_form .selectRegion').eq(1).find('option[value="'+region_code+'"]').attr('selected',true);*/
                        $('#edit_login_form select[name="s_region_code_info_id"]').find('option[value="'+code_info+'"]').attr('selected',true);
                   },
                   complete:function(){
                        $('.loading').hide();
                  },
                  error : function(err) {
                      alert("系统异常,请联系管理员!");
                      console.log(err);
                      $('.loading').hide();
                  }
              })
         })
        /*编辑提交*/
        $("#edit_login_form .register").click(function(){
            var login_no = $("#edit_login_form input[name='login_no']").val();
            var login_name = $("#edit_login_form input[name='login_name']").val();
            var password = $("#edit_login_form input[name='password']").val();
            var password_confirmation = $("#edit_login_form input[name='password_confirmation']").val();
            var valid_flag = $("#edit_login_form select[name='valid_flag'] option:selected").val();
            var created_at = $("#edit_login_form input[name='created_at']").val();
            var update_at = $("#edit_login_form input[name='update_at']").val();
            var valid_begin = $("#edit_login_form input[name='valid_begin']").val();
            var valid_end = $("#edit_login_form input[name='valid_end']").val();
            var describe = $("#edit_login_form input[name='describe']").val();
            var role_id = $("#edit_login_form select[name='role_id'] option:selected").val();
            var s_region_code_info_id = $("#edit_login_form select[name='s_region_code_info_id'] option:selected").val(); // 所属单位
            var group_id = $("#edit_login_form input[name='group_id']").val();
            /*判断区域最后一个选中的ID*/
           /* if($("#edit_login_form .selectRegion_form .selectRegion:nth-last-of-type(1) option").length > 0) {
                group_id = $("#edit_login_form .selectRegion_form .selectRegion:nth-last-of-type(1) option:selected").val();
                
            }else if($("#edit_login_form .selectRegion_form .selectRegion:nth-last-of-type(2) option").length > 0) {
                group_id = $("#edit_login_form .selectRegion_form .selectRegion:nth-last-of-type(2) option:selected").val();

            }else if($("#edit_login_form .selectRegion_form .selectRegion:nth-of-type(1) option").length > 0) {
                group_id = $("#edit_login_form .selectRegion_form .selectRegion:nth-of-type(1) option:selected").val();
            }*/
            $.ajax({
                type:"put",
                dataType:"json",
                url:"d_login_msgs/"+editID+".json",
                async:true,
                data :{
                d_login_msg:{
                        login_no:login_no,
                        login_name:login_name,
                        password:password,
                        password_confirmation:password_confirmation,
                        valid_flag:valid_flag,
                        created_at:created_at,
                        update_at:update_at,
                        valid_begin:valid_begin,
                        valid_end:valid_end,
                        describe:describe,
                        s_role_msg_id:role_id,
                        group_id:group_id,
                        s_region_code_info_id:s_region_code_info_id
                    }
                },
                beforeSend:function(){
                    $('.loading').show();
                },
                success:function(data){
                    if(data.status=="1") {
                        alert("修改成功")
                        $("#role_edit").modal("hide");
                        originPage();
                    }
                },
                complete:function(){
                    $('.loading').hide();
                },
                error:function(err){
                     alert("系统异常,请联系管理员!");
                     $('.loading').hide();
                }
            })
        });
        /*重置密码*/
         $("body").delegate("#d_login_msg .container .newsRight .listBanner span.reset","click",function(){
            var resetID = $(this).attr("value");
            if(confirm('确定重置密码？')){
                $.ajax({
                     type:"put",
                     dataType:"json",
                     url:"d_login_msgs/"+resetID+".json",
                     async:true,
                     data:{
                        d_login_msg:{
                            password_confirmation:'123',
                            password:'123',
                            id:resetID
                        }
                     },
                     beforeSend:function(){
                        $('.loading').show();
                     },
                     success:function(data){
                        console.log(data);
                        if(data.status == "1") {
                            alert("密码重置成功");
                        }else{
                             alert("密码重置失败");
                        }
                     },
                    complete:function(){
                        $('.loading').hide();
                    },
                     error:function(err){
                         alert("系统异常,请联系管理员!");
                         $('.loading').hide();
                     }
                })
            }
         })
       
           
        
        function originPage(page){
            var login_name = $(".public_search input[name='login_name']").val();
            var role_id = $(".public_search select[name='role_id'] option:selected").val();
             $.ajax({
                type:'get',
                url:'/d_login_msgs.json',
                dataType:'json',
                async:true,
                data:{
                    pageNow:page,
                },
                beforeSend:function(){
                    $('.loading').show();
                },
                success:function(data){
                    console.log(data);
                    var result = template("role_table",data);
                    var role_result = template("role_select",data);
                //    var search_role_select = template("search_role_select",data);
                //    $("#d_login_msg .public_search select[name='role_id']").html(search_role_select);//查询select渲染
                    $("#d_login_msg .container .newsRight .listBanner").html(result);
                    $("#d_login_msg #role_add select[name='role_id']").html(role_result);   //添加角色select渲染
                    $("#d_login_msg #role_edit select[name='role_id']").html(role_result);   //编辑角色select渲染
                    var group_region = template("group_region",data); //区域
                    $("#d_login_msg .region_station").html(group_region);  //添加--区域
                    var s_region_code_info = template("s_region_code_info",data);//所属单位
                    $("#d_login_msg select[name='s_region_code_info_id']").html(s_region_code_info,data);
                   /* $("body").delegate("#d_login_msg select.fatherFir_region","change",function(){
                        var father_region_id = $(this).val();
                        console.log(father_region_id);
                         $.ajax({
                            type:'get',
                            url:'d_login_msgs/region_codes',
                            dataType:'json',
                            async:true,
                            data:{
                                father_region_id:father_region_id,
                            },
                            beforeSend:function(){
                                $('.loading').show();
                            },
                            success:function(data){
                                console.log(data);
                                $('.loading').hide();
                                var group_regionSec = template("group_regionSec",data);
                                $("#new_login_form select[name='group_id']").eq(1).html(group_regionSec); //添加--二级区域
                                $("#edit_login_form select[name='group_id']").eq(1).html(group_regionSec);//编辑--二级区域
                            },
                            error:function(err){
                                console.log(err);
                                $('.loading').hide();
                                
                            }
                         })
                    })*/
                },
                complete:function(){
                    $('.loading').hide();
                },
                error:function(err){
                     alert("系统异常,请联系管理员!");
                     $(".loading").hide();
                }
            });
        }
        /*查询*/
        function originSearch(page){
            var login_name = $(".public_search input[name='login_name']").val();
            var role_id = $(".public_search select[name='role_id'] option:selected").val();
             $.ajax({
                type:'get',
                url:'/d_login_msgs.json',
                dataType:'json',
                async:true,
                data:{
                    pageNow:page,
                    login_name:login_name,
                    role_id:role_id
                },
                beforeSend:function(){
                    $('.loading').show();
                },
                success:function(data){
                    console.log(data);
                    var result = template("role_table",data);
                    var role_result = template("role_select",data);
                 //   var search_role_select = template("search_role_select",data);
                  //  $("#d_login_msg .public_search select[name='role_id']").html(search_role_select);//查询角色select渲染
                    $("#d_login_msg .container .newsRight .listBanner").html(result);
                    $("#d_login_msg #role_add select[name='role_id']").html(role_result);   //添加角色select渲染
                    $("#d_login_msg #role_edit select[name='role_id']").html(role_result);   //编辑角色select渲染
                    
                },
                complete:function(){
                    $('.loading').hide();
                },
                error:function(err){
                     alert("系统异常,请联系管理员!");
                     $(".loading").hide();
                }
            });
        }
         $.ajax({
                type:'get',
                url:'/d_login_msgs.json',
                dataType:'json',
                async:true,
                beforeSend:function(){
                    $('.loading').show();
                },
                success:function(data){
                    console.log(data);
                    var count = data.count;
                    $(".yn_public_page .pageTotal a").text(count);
                    console.log(count)
                    var result = template("role_table",data);
                    var role_result = template("role_select",data);
                    var search_role_select = template("search_role_select",data);
                    $("#d_login_msg .public_search select[name='role_id']").html(search_role_select);//查询select渲染
                    $("#d_login_msg .container .newsRight .listBanner").html(result);
                    $("#d_login_msg #role_add select[name='role_id']").html(role_result);   //添加角色select渲染
                    $("#d_login_msg #role_edit select[name='role_id']").html(role_result);   //编辑角色select渲染
                    $("#page").initPage(count, '1', originPage);
                },
                complete:function(){
                    $('.loading').hide();
                },
                error:function(err){
                     alert("系统异常,请联系管理员!");
                     $(".loading").hide();
                }
            });
        })
    })