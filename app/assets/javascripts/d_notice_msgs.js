   $(function(){
        alert("告警页面加载")
        $(".loading").show();
        var $height;
        var $scrollHeight;
        $(window).resize(function(){
            $height = $("#d_notice_msg").height();
            console.log($height)
            $scrollHeight = $height - 200;
            console.log($scrollHeight);
            $("#d_notice_msg .container .newsRight .listBanner").css("max-height",$scrollHeight);
        })
        $(document).ready(function(){
            $height = $("#d_notice_msg").height();
            console.log($height)
            $scrollHeight = $height - 200;
            console.log($scrollHeight);
            $("#d_notice_msg .container .newsRight .listBanner").css("max-height",$scrollHeight);
            var time = new Date();
            var year = time.getFullYear();
            var month = time.getMonth() + 1;
            var day = time.getDate();
            var hour = time.getHours();
            var minnute = time.getMinutes();
            var second = time.getSeconds();
            var result = year + '-' + (month < 10 ? '0' + month : month) + '-'
			+ (day < 10 ? '0' + day : day);
            var resultTime = year + '-' + (month < 10 ? '0' + month : month) + '-'
			+ (day < 10 ? '0' + day : day) + ' ' + (hour < 10 ? '0' + hour : hour) +':'
            + (minnute < 10 ? '0' + minnute : minnute)+':'+(second < 10 ? '0' + second : second);
	        $('.datetimepicker').val(result);
	        $(".modal input[name='created_at']").val(resultTime);
            $(".modal input[name='updated_at']").val(resultTime);
            $(".modal input[name='recive_tiem']").val(resultTime);
            $(".modal input[name='send_time']").val(resultTime);
            $('.datetimepicker').datetimepicker({
                format : "Y-m-d",
                minDate : year + '-01-01',
                maxDate : year + '-12-31',
                todayButton : true,
                timepicker : false
            });
            /*新建验证*/
            $('#new_notice_form').bootstrapValidator({
              message : 'This value is not valid',
              fields : {
                author : {
                  message : '发布公告者',
                  validators : {
                    notEmpty : {
                      message : '发布公告者不能为空'
                    }
                  }
                },
                title:{
                     message : '公告标题',
                     validators : {
                        notEmpty : {
                            message : '公告标题不能为空'
                        }
                    }
                },
                 contents:{
                     message : '公告内容',
                     validators : {
                        notEmpty : {
                            message : '公告内容不能为空'
                        }
                    }
                },
              }
            });
             /*编辑验证*/
            $('#edit_notice_form').bootstrapValidator({
              message : 'This value is not valid',
              fields : {
                author : {
                  message : '发布公告者',
                  validators : {
                    notEmpty : {
                      message : '发布公告者不能为空'
                    }
                  }
                },
                title:{
                     message : '公告标题',
                     validators : {
                        notEmpty : {
                            message : '公告标题不能为空'
                        }
                    }
                },
                 contents:{
                     message : '公告内容',
                     validators : {
                        notEmpty : {
                            message : '公告内容不能为空'
                        }
                    }
                },
              }
            });
        /*查询*/
        $("#d_notice_msg .public_search .submit").click(function(){
            var title = $("#d_notice_msg .public_search input[name='title']").val();
            $.ajax({
                type:"get",
                url:"d_notice_msgs.json",
                dataType:"json",
                async:true,
                data:{
                    pageNow:1,
                    title:title
                },
                beforeSend:function(){
                    $('.loading').show();
                },
                success:function(data){
                    console.log(data);
                    var result = template("role_table",data);
                    $("#d_notice_msg .container .newsRight .listBanner").html(result);
                    var count = data.count;
                    $("#page").initPage(count, '1', originSearch);
                },
                complete:function(){
                    $('.loading').hide();
                },
                error:function(err){
                    alert("系统异常,请联系管理员!");
                    console.log(err);
                    $('.loading').hide();

                }
            })
        })
          /*添加提交*/
          $('#new_notice_form .register').click(function() {
              var addValidator = $('#new_notice_form').data('bootstrapValidator');
              addValidator.validate();
                var title = $("#new_notice_form input[name='title']").val();
                var author = $("#new_notice_form input[name='author']").val();
                var contents = $("#new_notice_form input[name='contents']").val();
                var notice_type = $("#new_notice_form input[name='notice_type']").val();
                var notice_level = $("#new_notice_form input[name='notice_level']").val();
                var send_time = $("#new_notice_form input[name='send_time']").val();
                var recive_tiem = $("#new_notice_form input[name='recive_tiem']").val();
                var created_at = $("#new_notice_form input[name='created_at']").val();
                var updated_at = $("#new_notice_form input[name='updated_at']").val();
              if(addValidator.isValid()){
                  $.ajax({
                      url : '/d_notice_msgs.json',
                      type : 'post',
                      dataType : "json",
                      async:true,
                      data :{
                        d_notice_msg:{
                          title:title,
                          author:author,
                          contents:contents,
                          notice_type:notice_type,
                          notice_level:notice_level,
                          send_time:send_time,
                          recive_tiem:recive_tiem,
                          created_at:created_at,
                          updated_at:updated_at,
                        }
                      },
                     beforeSend:function(){
                        $('.loading').show();
                     },
                      success : function(data) {
                          console.log(data);
                          alert(data.status)
                          originPage();
                          $("#notice_add").modal("hide");
                      },
                    complete:function(){
                        $('.loading').hide();
                     },
                      error : function(err) {
                          alert("系统异常,请联系管理员!");
                          console.log(err);
                          $('.loading').hide();
                      }
                  })
              }
              $('#new_notice_form').data('bootstrapValidator').resetForm(true);
          })
   /*点击编辑*/
   var editID;
   $("body").delegate("#d_notice_msg .container .newsRight .listBanner .banner .edit","click",function(){
        editID = $(this).attr("value");
        $.ajax({
             type:"get",
             url:"/d_notice_msgs/"+editID+"/edit.json",
             dataType:"json",
             async:true,
             beforeSend:function(){
                $(".loading").show();
             },
             success:function(data){
                console.log(data);
                var author = data.author;
                var contents = data.contents;
                var created_at = data.created_at;
                var notice_level = data.notice_level;
                var notice_type = data.notice_type;
                var recive_tiem = data.recive_tiem;
                var send_time = data.send_time;
                var title = data.title;
                var updated_at = data.updated_at;
                $("#notice_edit #edit_notice_form input[name='author']").val(author);
                $("#notice_edit #edit_notice_form input[name='contents']").val(contents);
                $("#notice_edit #edit_notice_form input[name='created_at']").val(created_at);
                $("#notice_edit #edit_notice_form input[name='notice_level']").val(notice_level);
                $("#notice_edit #edit_notice_form input[name='notice_type']").val(notice_type);
                $("#notice_edit #edit_notice_form input[name='recive_tiem']").val(recive_tiem);
                $("#notice_edit #edit_notice_form input[name='recive_tiem']").val(send_time);
                $("#notice_edit #edit_notice_form input[name='recive_tiem']").val(updated_at);
                $("#notice_edit #edit_notice_form input[name='title']").val(title);
              
             },
             complete:function(){
                $(".loading").hide();
             },
             error:function(err){
                console.log(err);
                $(".loading").hide();
                alert("系统异常,请联系管理员!");
             }
        })
   })
   /*编辑提交*/
    $("#edit_notice_form .register").click(function(){
        var editValidator = $('#edit_notice_form').data('bootstrapValidator');
        editValidator.validate();
        var title = $("#edit_notice_form input[name='title']").val();
        var author = $("#edit_notice_form input[name='author']").val();
        var contents = $("#edit_notice_form input[name='contents']").val();
        var notice_type = $("#edit_notice_form input[name='notice_type']").val();
        var notice_level = $("#edit_notice_form input[name='notice_level']").val();
        var send_time = $("#edit_notice_form input[name='send_time']").val();
        var recive_tiem = $("#edit_notice_form input[name='recive_tiem']").val();
        var created_at = $("#edit_notice_form input[name='created_at']").val();
        var updated_at = $("#edit_notice_form input[name='updated_at']").val();
        if(editValidator.isValid()){
            $.ajax({
                type:"put",
                url:"/d_notice_msgs/"+editID+".json",
                dataType:"json",
                async:true,
                data :{
                    d_notice_msg:{
                        title:title,
                        author:author,
                        contents:contents,
                        notice_type:notice_type,
                        notice_level:notice_level,
                        send_time:send_time,
                        recive_tiem:recive_tiem,
                        created_at:created_at,
                        updated_at:updated_at,
                    }
                },
                 beforeSend:function(){
                    $('.loading').show();
                },
                success:function(data){
                    console.log(data);
                      if(data.status == "1"){
                            alert("修改成功");
                            $("#notice_edit").modal("hide");
                            originPage();
                        }
                },
                 complete:function(){
                    $('.loading').hide();
                },
                error:function(err){
                    console.log(err);
                    alert("系统异常,请联系管理员!");
                    $('.loading').hide();
                }
            })
        }
 
    })
    /*删除*/
    $("body").delegate("#d_notice_msg .container .newsRight .listBanner .banner .delete","click",function(){
        var deleteID = $(this).attr("value");
        if(confirm("确定删除改公告?")){
            $.ajax({
                    type:'delete',
                    url:"/d_notice_msgs/"+deleteID+".json",
                    dataType:'json',
                    async:true,
                    beforeSend:function(){
                        $('.loading').show();
                    },
                    success:function(data){
                        console.log(data);
                        if(data.status == "1"){
                            alert("删除成功");
                            originPage();
                        }else{
                            alert("删除失败");
                        }
                    },
                    complete:function(){
                        $('.loading').hide();
                    },
                    error:function(err){
                        console.log(err);
                        alert("系统异常,请联系管理员!");
                        $('.loading').hide();
                    }
            })
        }

    })
    function originPage(page){
            var title = $(".public_search input[name='title']").val();
             $.ajax({
                type:'get',
                url:'/d_notice_msgs.json',
                dataType:'json',
                async:true,
                data:{
                    pageNow:page,
                },
                beforeSend:function(){
                    $('.loading').show();
                },
                success:function(data){
                    console.log(data);
                    var count = data.count;
                    $(".yn_public_page .pageTotal a").text(count);
                    var result = template("role_table",data);
                    $("#d_notice_msg .container .newsRight .listBanner").html(result);
                },
               complete:function(){
                    $('.loading').hide();
                },
                error:function(err){
                     alert("系统异常,请联系管理员!");
                     $('.loading').hide();
                }
            });
        }
        /*查询*/
        function originSearch(pageSearch){
            var title = $(".public_search input[name='title']").val();
             $.ajax({
                type:'get',
                url:'/d_notice_msgs.json',
                dataType:'json',
                async:true,
                data:{
                    pageNow:pageSearch,
                    title:title
                },
                beforeSend:function(){
                    $('.loading').show();
                },
                success:function(data){
                    console.log(data);
                    var count = data.count;
                    $(".yn_public_page .pageTotal a").text(count);
                    var result = template("role_table",data);
                    $("#d_notice_msg .container .newsRight .listBanner").html(result);
                },
               complete:function(){
                    $('.loading').hide();
                },
                error:function(err){
                     alert("系统异常,请联系管理员!");
                     $('.loading').hide();
                }
            });
        }
         $.ajax({
                type:'get',
                url:'/d_notice_msgs.json',
                dataType:'json',
                async:true,
                beforeSend:function(){
                    $('.loading').show();
                },
                success:function(data){
                    console.log(data);
                    var count = data.count;
                    $(".yn_public_page .pageTotal a").text(count);
                    console.log(count)
                    var result = template("role_table",data);
                    $("#d_notice_msg .container .newsRight .listBanner").html(result);
                    $("#page").initPage(count, '1', originPage);
                },
                complete:function(){
                    $('.loading').hide();
                },
                error:function(err){
                     alert("系统异常,请联系管理员!");
                     $('.loading').hide();
                }
            });
        })
    })