class RootHomeController < ApplicationController
  before_action :authenticate_user

  def index
    @ihomes = Ihome.all
    @role_msgs =  @current_user.s_role_msg.role_name
    @s_subsyst_lists = SSubsystList.all
  end

end