class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def set_error_msg( error_msg )
    @error_msg = error_msg
    flash[:notice] = error_msg
  end

  def current_user
    Rails.logger.debug( "当前用户的SessionCode: " + session[:session_code].to_s )
    @current_user ||= DLoginMsg.find_by( "session" => session[:session_code].to_s )
  end
  
  def signed_in?
    !current_user.nil?
  end

  def authenticate_user
    ## 转存公共参数
    @curr_page      = params[:curr_page]
    @total_page     = params[:total_page]
    @size_pre_page  = params[:size_pre_page]
    
    
    Rails.logger.debug( "当前用户信息: #{current_user}" )
    if current_user.nil?
      redirect_to "/sessions/new", notice: '用户信息不存在，请注册？'
    end
  end
  
end
