class SessionsController < ApplicationController
  before_action :set_session, only: [:show, :edit, :update, :destroy]
  #before_filter :authenticate_user, only: [:destroy]
  skip_before_action :verify_authenticity_token, only: [:app_create]

  # GET /sessions/new
  def new

    @dloginmsg = DLoginMsg.new
    
    ## 登录布局
    render layout: "login_layout"
  end

  # POST /sessions
  # POST /sessions.json
  def create
    login_no = params[:login_no]
    password = params[:password]
    if login_no.blank? || password.blank? 
      set_error_msg( "请提交完成参数" )
      
      render :new, layout: "login_layout"
      return
    end
  
    Rails.logger.debug( "检查工号" )
    @dloginmsg = DLoginMsg.find_by( "login_no" => login_no,"valid_flag" => "1" )
    
    if @dloginmsg.nil?
      set_error_msg( "无此工号" )
      
      render :new, layout: "login_layout"
      return
    end
    
    Rails.logger.debug( "检查密码 " )
    unless @dloginmsg.authenticate( password )
      set_error_msg( "密码错误" )
      
      render :new, layout: "login_layout"
      return
    end
    
    # 写日志
    wLoginOpr = WLoginOpr.new
    wLoginOpr.op_code   = "sessions|create"
    wLoginOpr.op_time   = Time.now.to_s
    wLoginOpr.login_no  = login_no 
    wLoginOpr.ip_addr   = request.ip.to_s 
    wLoginOpr.op_note   = "工号登录"
    wLoginOpr.op_describe = "工号登录"
    wLoginOpr.save

    
    Rails.logger.debug( "账号验证成功 " )
    respond_to do |format|
      if sign_in( @dloginmsg )
        Rails.logger.debug( "转向home页 " )
        format.html { redirect_to '/ihomes' }
        format.json { render json: {"loginName"=>@dloginmsg.login_name,user:@dloginmsg}, status: :ok }
      else
        Rails.logger.debug( "session 建立失败 " )
        format.html { render :new, layout: "login_layout" }
        format.json { render json: {"RETCODE"=>"R40", "RETMSG"=>@dloginmsg.errors}, status: :ok }
      end
    end
  end

  # WEBAPP: POST /app_sessions
  # WEBAPP: POST /app_sessions.json
  def app_create
    begin
      retCode = true
      login_no = params[:login_no]
      password = params[:password]
      
      if login_no.blank? || password.blank? 
        error_msg = "请提交完成参数"
        
        retCode = false
        return
      end
    
      Rails.logger.debug( "检查工号" )
      @dloginmsg = DLoginMsg.find_by( "login_no" => login_no )
      if @dloginmsg.nil?
        error_msg = "无此工号"
        
        retCode = false
        return
      end
      
      Rails.logger.debug( "检查密码 " )
      unless @dloginmsg.authenticate( password )
        error_msg = "密码错误"
        
        retCode = false
        return
      end
      
      # 写日志
      wLoginOpr = WLoginOpr.new
      wLoginOpr.op_code   = "sessions|create"
      wLoginOpr.op_time   = Time.now.to_s
      wLoginOpr.login_no  = login_no 
      wLoginOpr.ip_addr   = request.ip.to_s 
      wLoginOpr.op_note   = "工号登录"
      wLoginOpr.op_describe = "工号登录"
      wLoginOpr.save

      Rails.logger.debug( "账号验证成功 " )
      retCode = sign_in( @dloginmsg )
      unless retCode
        error_msg = @dloginmsg.errors
        Rails.logger.debug( error_msg.inspect )
        
        return
      end
      
    ensure
      if retCode
        Rails.logger.debug( "login ok" )
        render json: {"RETCODE"=>"R20", "RETMSG"=>"OK", "session_code"=>session[:session_code]}, status: :ok, layout: false
      else
        Rails.logger.debug( "login error: " + error_msg + "" )
        render json: {"RETCODE"=>"R40", "RETMSG"=>error_msg}, status: :ok, layout: false
      end
    end
  end

  # DELETE /sessions/1
  # DELETE /sessions/1.json
  def destroy
    current_user.update( "session" => "---------------" )
    session[:session_code] = "----"
    
    respond_to do |format|
      format.html { redirect_to "/sessions/new", notice: 'Session was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_session
      @dloginmsg = DLoginMsg.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def session_params
      params.require(:dloginmsg).permit(:login_no, :password)
    end
    
    def sign_in( dloginmsg )
      Rails.logger.debug( "用户登录使用SessionID：" + session[:session_id].to_s )
      session[:session_code] = dloginmsg.new_session
      Rails.logger.debug( "用户登录创建SessionCode：" + session[:session_code].to_s )
      if dloginmsg.update( "session" => session[:session_code].to_s, "ip_address" => request.ip )
        current_user = dloginmsg
        
        # session 保存常用数据
        
        return true
      else
        return false
      end
    end
end
