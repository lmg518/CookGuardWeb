class DLoginMsgsController < ApplicationController
  before_filter :authenticate_user
  before_action :set_d_login_msg, only: [:show, :edit, :update, :destroy]

  # GET /d_login_msgs
  # GET /d_login_msgs.json
  def index
    @d_region_code  = SRegionCode.all #区域
    @s_region_code_info = SRegionCodeInfo.select(:id,:unit_name) #单位
    login_name = params[:login_name]
    role_id = params[:role_id]
    pageNow = params[:pageNow]
    pageSize = 10
    users = DLoginMsg.new
    @roles = SRoleMsg.where('valid_flag = 1')
    users = DLoginMsg.by_user_id(role_id).by_user_name(login_name)
    @d_login_msgs = users.page(pageNow).per(pageSize)
    @count = users.count
    s = @count%pageSize
    if s == 0
      @page = @count/pageSize
    else
      @page = (@count-s)/pageSize+1
    end 
      str = Time.now
      Rails.logger.info str.strftime("%Y-%m-%d %H:%M:%S")
    end

  # GET /d_login_msgs/1
  # GET /d_login_msgs/1.json
  def show
  end

  # GET /d_login_msgs/new
  def new
    @d_login_msg    = DLoginMsg.new
    
  end

  # GET /d_login_msgs/1/edit
  def edit
    id = params[:id]
    unless id.nil?
      @d_login_msg = DLoginMsg.find_by(:id =>id)
      @s_region_code = SRegionCode.select(:id,:region_name,:father_region_id).find_by(:id =>@d_login_msg.group_id)
      @father_region_code = SRegionCode.select(:id,:region_name).find_by(:id =>@s_region_code.father_region_id)
      @code_info = SRegionCodeInfo.select(:id,:unit_name).find_by(:id =>@d_login_msg.s_region_code_info_id)
    end
  end

  # POST /d_login_msgs
  # POST /d_login_msgs.json
  def create
    retCode = true
    @d_login_msg = DLoginMsg.new(d_login_msg_params)
    # 写日志
    wLoginOpr = WLoginOpr.new
    wLoginOpr.op_code   = "d_login_msgs|create"
    wLoginOpr.op_time   = Time.now.to_s
    wLoginOpr.login_no  = current_user.login_no
    wLoginOpr.ip_addr   = current_user.ip_address
    wLoginOpr.op_note   = "增加工号"
    wLoginOpr.op_describe = "增加工号，工号名称：" + d_login_msg_params[:login_name]
    
    login_no = d_login_msg_params[:login_no]
    login = DLoginMsg.where('login_no = ?',login_no)
    if login.blank?
      @d_login_msg.transaction do
        retCode = @d_login_msg.save
        retCode = wLoginOpr.save if retCode
      end

      respond_to do |format|
        if retCode
          format.html { redirect_to "/d_login_msgs" }
          format.json { render json:{'status': '新增成功'}}
        else
          format.html { render :new }
          format.json { render json: @d_login_msg.errors, status: :unprocessable_entity }
        end
      end
    else       
      render json:{'status': '工号重复'}
    end 
  end

  # PATCH/PUT /d_login_msgs/1
  # PATCH/PUT /d_login_msgs/1.json
  def update
    retCode = true
    d_login_msg = DLoginMsg.find(params[:id])    
     # 写日志
    wLoginOpr = WLoginOpr.new
    wLoginOpr.op_code   = "d_login_msgs|update"
    wLoginOpr.op_time   = Time.now.to_s
    wLoginOpr.login_no  = current_user.login_no
    wLoginOpr.ip_addr   = current_user.ip_address
    wLoginOpr.op_note   = "编辑工号"
    wLoginOpr.op_describe = "编辑工号，" + "工号编码：" + d_login_msg.login_no + ", 工号名称：" + d_login_msg.login_name
    
    @d_login_msg.transaction do
      retCode = @d_login_msg.update(d_login_msg_params)
      retCode = wLoginOpr.save if retCode
    end
    
    respond_to do |format|
      if retCode
        format.html { redirect_to "/d_login_msgs" }
        format.json { render json:{'status': '1'} }
      else
        format.html { render :edit }
        format.json { render json: @d_login_msg.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /d_login_msgs/1
  # DELETE /d_login_msgs/1.json
  def destroy
    @d_login_msg.destroy
    respond_to do |format|
      format.html { redirect_to d_login_msgs_url, notice: 'D login msg was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  #GET /d_login_msgs/region_codes
  def region_codes
      father_id = params[:father_region_id]
      @s_region_code = SRegionCode.where(:father_region_id =>father_id)
      render json: {d_region_code: @s_region_code}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_d_login_msg
      @d_login_msg = DLoginMsg.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def d_login_msg_params
      params.require(:d_login_msg).permit( :login_no, :login_name, :group_id, :s_role_msg_id, :password, :password_confirmation, :password_digest, :valid_flag, :valid_begin, :valid_end, :ip_address, :describe ,:s_region_code_info_id)
    end
end
