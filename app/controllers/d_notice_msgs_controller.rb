class DNoticeMsgsController < ApplicationController

  def index
    title = params[:title]
    pageNow = params[:pageNow]
    pageSize = 10
    if title.nil? || title.empty?
      notices = DNoticeMsg.all
    else 
      notices = DNoticeMsg.where("title LIKE ?","%#{title}%")
    end
    
    @d_notice_msgs = notices.page(pageNow).per(pageSize)
    @count = notices.count
    s = @count%pageSize
    if s == 0
      @page = @count/pageSize
    else
      @page = (@count-s)/pageSize+1
    end 
  end

  def create
    d_notice_msg = DNoticeMsg.new(d_notice_msg_params)
    
    # 写日志
    wLoginOpr = WLoginOpr.new
    wLoginOpr.op_code   = "d_notice_msg|create"
    wLoginOpr.op_time   = Time.now.to_s
    wLoginOpr.login_no  = current_user.login_no
    wLoginOpr.ip_addr   = current_user.ip_address
    wLoginOpr.op_note   = "增加公告"
    wLoginOpr.op_describe = current_user.login_name + "：增加公告"

    retCode = true
    d_notice_msg.transaction do
      retCode = d_notice_msg.save!
      retCode = wLoginOpr.save! if retCode
    end
    if retCode
      self.index
      render json:{'status': '新增成功'}
    else
      render json: d_notice_msg.errors, status: :unprocessable_entity 
    end
  end 

  def edit
    @d_notice_msg = set_d_notice_msg    
  end

  def update
    retCode = true
    d_notice_msg = set_d_notice_msg
     # 写日志
    wLoginOpr = WLoginOpr.new
    wLoginOpr.op_code   = "d_notice_msg|update"
    wLoginOpr.op_time   = Time.now.to_s
    wLoginOpr.login_no  = current_user.login_no
    wLoginOpr.ip_addr   = current_user.ip_address
    wLoginOpr.op_note   = "编辑公告"
    wLoginOpr.op_describe = "编辑公告，" + "工号编码：" + current_user.login_no + ", 工号名称：" + current_user.login_name
   
     @d_notice_msg.transaction do
      retCode = @d_notice_msg.update(d_notice_msg_params)
      retCode = wLoginOpr.save if retCode
    end

    if retCode
      render json:{'status': '1'}
    else
      render json: @d_notice_msg.errors, status: :unprocessable_entity 
    end
  end 

  def destroy
    ret = set_d_notice_msg.destroy!
    if ret
      render json:{'status': '1'}
    else
      render json:{'status': '0'}
    end
    # respond_to do |format|
    #   format.html { redirect_to s_role_msgs_url, notice: 'S role msg was successfully destroyed.' }
    #   format.json { render json:{'status': '1'} }
    # end
  end

  private

    def set_d_notice_msg
      @d_notice_msg = DNoticeMsg.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def d_notice_msg_params
      params.require(:d_notice_msg).permit(:title, :author, :contents, :notice_type, :notice_level, :send_time, :recive_tiem, :created_at, :updated_at)
    end


end