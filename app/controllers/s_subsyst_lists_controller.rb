class SSubsystListsController < ApplicationController
  before_filter :authenticate_user
  before_action :set_s_subsyst_liste, only: [:show, :edit, :update, :destroy]

  # GET /s_subsyst_listes
  # GET /user_subsys_lists.json
  def user_subsys
    
    # 业务系统链接
    @s_subsyst_lists = current_user.s_role_msg.s_subsyst_list
    @s_subsyst_lists.each do |s_subsyst_list|
        s_subsyst_list.sys_url = s_subsyst_list.sys_url + "?session=" + session[:session_code].to_s 
    
    end
    @subsyst_list = SSubsystList.where.not(:sys_name =>'发布公告')
    respond_to do |format|
      #format.html { redirect_to "/sessions/new", notice: 'Session was successfully destroyed.' }
      format.json { render :user_subsyst, status: :ok }
    end
  end
  
  # GET /s_subsyst_listes
  # GET /s_subsyst_listes.json
  def index
    @s_subsyst_lists = SSubsystList.all
  end

  # GET /s_subsyst_listes/1
  # GET /s_subsyst_listes/1.json
  def show
  end

  # GET /s_subsyst_listes/new
  def new
    @s_subsyst_list = SSubsystList.new
  end

  # GET /s_subsyst_listes/1/edit
  def edit
  end

  # POST /s_subsyst_listes
  # POST /s_subsyst_listes.json
  def create
    @s_subsyst_list = SSubsystList.new(s_subsyst_list_params)

    respond_to do |format|
      if @s_subsyst_liste.save
        format.html { redirect_to @s_subsyst_list, notice: 'S subsyst liste was successfully created.' }
        format.json { render :show, status: :created, location: @s_subsyst_list }
      else
        format.html { render :new }
        format.json { render json: @s_subsyst_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /s_subsyst_listes/1
  # PATCH/PUT /s_subsyst_listes/1.json
  def update
    respond_to do |format|
      if @s_subsyst_liste.update(s_subsyst_list_params)
        format.html { redirect_to @s_subsyst_list, notice: 'S subsys liste was successfully updated.' }
        format.json { render :show, status: :ok, location: @s_subsyst_list }
      else
        format.html { render :edit }
        format.json { render json: @s_subsyst_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /s_subsyst_listes/1
  # DELETE /s_subsyst_listes/1.json
  def destroy
    @s_subsyst_liste.destroy
    respond_to do |format|
      format.html { redirect_to s_subsyst_lists_url, notice: 'S subsys liste was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_s_subsyst_list
      @s_subsyst_list = SSubsystList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def s_subsyst_liste_params
      params.require(:s_subsyst_list).permit(:sys_name, :sys_url, :mark)
    end
end
