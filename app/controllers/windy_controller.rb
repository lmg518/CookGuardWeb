class WindyController < ApplicationController

  def index
    time = Time.parse(params[:data_time]).beginning_of_hour
    wether_datum = DWetherDatum.where(:real_time => time.strftime("%Y-%m-%d %H:00:00"))
    if wether_datum.blank?
      wether_datum = DWetherDatum.where(:real_time => (time - 1.hours).strftime("%Y-%m-%d %H:00:00"))
    end
    @all_data_arr =[]
    wether_datum.each do |wether|
      station = DWeatherStation.find_by(:station_id => wether.station_id)
      win_d_inst_max = wether.win_d_inst_max
      next if fix_num(win_d_inst_max) == 0
      next if fix_num(win_d_inst_max) > 360
      @all_data_arr << [station.lat.to_f,
                        station.lng.to_f,
                        fix_num(wether.win_d_inst_max),
                        fix_num(wether.win_s_inst_max),
                        fix_num(wether.rhu),
                        fix_num(wether.tem),
                        fix_num(wether.pre_1h)]
    end
    @all_data_arr
    render json: {windy: @all_data_arr}
  end

  def fix_num(obj)
    if obj.to_i == 999999
      return 0
    else
      return obj.to_f
    end
  end

end