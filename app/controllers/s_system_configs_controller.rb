class SSystemConfigsController < ApplicationController

    def index
        @s_system_configs = SSystemConfig.all
        @configs=Hash.new
        @s_system_configs.each do |f|
            @configs[f.code] = f.name
        end
    end

    def show
    end

end