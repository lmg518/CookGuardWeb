class SRegionCodeInfosController < ApplicationController
    before_action :get_code_info, only: [:show,:edit,:update,:destory]

    def index
        @d_region_code  = SRegionCode.where(:father_region_id => '9999') #一级区域
        @region_code = SRegionCodeInfo.by_info_name(params[:unit_name]).order(:created_at => :desc)
        @region_code_info = @region_code.page(params[:page]).per(params[:per])
    end

    def new
        @region_code_info = SRegionCodeInfo.new
    end


    def create
        @region_code_info = SRegionCodeInfo.new(create_code_info)
        respond_to do |format|
            if @region_code_info.save
              format.html { redirect_to "/s_region_code_infos" }
              format.json { render json:{'status': '新增成功'}}
            else
              format.html { render :new }
              format.json { render json: @region_code_info.errors, status: :unprocessable_entity }
            end
        end
    end

    #GET /s_region_code_infos/get_ids
    def get_ids
        @region_code_info = SRegionCodeInfo.select(:id,:unit_name)
        render json:{:region_code_info =>@region_code_info}
    end

    def edit

    end

    def update
        @region_code_info = @region_info.update(create_code_info)
        respond_to do |format|
            if @region_code_info.present?
                format.html { redirect_to "/s_region_code_infos" }
                format.json { render json:{'status': '修改成功'}}
            else
                format.html { render :edit }
                format.json { render json: @region_code_info.errors, status: :unprocessable_entity }
            end
        end
    end

    def destroy
        respond_to do |format|
            if @region_info.destroy
                format.html { redirect_to "/s_region_code_infos" }
                format.json { render json:{'status': '删除成功'}}
            else
                format.html { redirect_to "/s_region_code_infos" }
                format.json { render json:{'status': '删除失败'} }
            end
        end

    end
    
    private
    def get_code_info
        @region_info = SRegionCodeInfo.find(params[:id])
    end

    def create_code_info
        params.require(:region_code_info).permit(:linkman,:linkman_tel,:unit_address,:unit_name,:unit_code,:status,:station_id,:s_region_code_id)
        
    end

end