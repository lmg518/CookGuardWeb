class IhomesController < ApplicationController
  before_filter :authenticate_user
  before_action :set_ihome, only: [:show, :edit, :update, :destroy]
  # before_action :current_user

  # GET /ihomes
  # GET /ihomes.json
  def index
    @ihomes = Ihome.all
    @role_msgs =  @current_user.s_role_msg.role_name
    @s_subsyst_lists = SSubsystList.all
    ## 登录布局
    render layout: "home_iframe"
  end

  # GET /ihomes/1
  # GET /ihomes/1.json
  def show
  end

  # GET /ihomes/new
  def new
    @ihome = Ihome.new
  end

  # GET /ihomes/1/edit
  def edit
  end

  # POST /ihomes
  # POST /ihomes.json
  def create
    @ihome = Ihome.new(ihome_params)

    respond_to do |format|
      if @ihome.save
        format.html { redirect_to @ihome, notice: 'Ihome was successfully created.' }
        format.json { render :show, status: :created, location: @ihome }
      else
        format.html { render :new }
        format.json { render json: @ihome.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ihomes/1
  # PATCH/PUT /ihomes/1.json
  def update
    respond_to do |format|
      if @ihome.update(ihome_params)
        format.html { redirect_to @ihome, notice: 'Ihome was successfully updated.' }
        format.json { render :show, status: :ok, location: @ihome }
      else
        format.html { render :edit }
        format.json { render json: @ihome.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ihomes/1
  # DELETE /ihomes/1.json
  def destroy
    @ihome.destroy
    respond_to do |format|
      format.html { redirect_to ihomes_url, notice: 'Ihome was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ihome
      @ihome = Ihome.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ihome_params
      params.fetch(:ihome, {})
    end
end
