Rails.application.routes.draw do

  mount CookV1Api => '/api/v1'

  resources :s_subsyst_lists
  get 'user_subsyst_list' => 's_subsyst_lists#user_subsys'
  resources :sessions, only:[:new, :create, :destroy]
  resources :d_login_msgs do
    get "region_codes", on: :collection
  end

  resources :s_role_msgs 
  
  resources :ihomes

  resources :d_notice_msgs

  resources :d_login_ips #限制登陆IP

  resources :s_region_code_infos do
    get "get_ids", on: :collection
  end

  resources :windy

  resources :root_home

  #系统页面配置 接口
  resources :s_system_configs





  get "GIS_map" => "gis_map#index"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  root 'ihomes#index'


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
