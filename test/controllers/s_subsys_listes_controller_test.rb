require 'test_helper'

class SSubsysListesControllerTest < ActionController::TestCase
  setup do
    @s_subsys_liste = s_subsys_listes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:s_subsys_listes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create s_subsys_liste" do
    assert_difference('SSubsysListe.count') do
      post :create, s_subsys_liste: { mark: @s_subsys_liste.mark, sys_name: @s_subsys_liste.sys_name, sys_url: @s_subsys_liste.sys_url }
    end

    assert_redirected_to s_subsys_liste_path(assigns(:s_subsys_liste))
  end

  test "should show s_subsys_liste" do
    get :show, id: @s_subsys_liste
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @s_subsys_liste
    assert_response :success
  end

  test "should update s_subsys_liste" do
    patch :update, id: @s_subsys_liste, s_subsys_liste: { mark: @s_subsys_liste.mark, sys_name: @s_subsys_liste.sys_name, sys_url: @s_subsys_liste.sys_url }
    assert_redirected_to s_subsys_liste_path(assigns(:s_subsys_liste))
  end

  test "should destroy s_subsys_liste" do
    assert_difference('SSubsysListe.count', -1) do
      delete :destroy, id: @s_subsys_liste
    end

    assert_redirected_to s_subsys_listes_path
  end
end
