require 'test_helper'

class IhomesControllerTest < ActionController::TestCase
  setup do
    @ihome = ihomes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ihomes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ihome" do
    assert_difference('Ihome.count') do
      post :create, ihome: {  }
    end

    assert_redirected_to ihome_path(assigns(:ihome))
  end

  test "should show ihome" do
    get :show, id: @ihome
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ihome
    assert_response :success
  end

  test "should update ihome" do
    patch :update, id: @ihome, ihome: {  }
    assert_redirected_to ihome_path(assigns(:ihome))
  end

  test "should destroy ihome" do
    assert_difference('Ihome.count', -1) do
      delete :destroy, id: @ihome
    end

    assert_redirected_to ihomes_path
  end
end
