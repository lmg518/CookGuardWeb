class CreateDLoginIps < ActiveRecord::Migration
  def change
    create_table :d_login_ips do |t|
      t.string :login_name
      t.string :ip
      
      t.timestamps
    end
  end
end
