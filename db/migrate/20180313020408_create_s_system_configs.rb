class CreateSSystemConfigs < ActiveRecord::Migration
  def change
    create_table :s_system_configs do |t|
	  t.string :code
	  t.string :name
	  t.string :system_type
      t.timestamps
    end
  end
end
