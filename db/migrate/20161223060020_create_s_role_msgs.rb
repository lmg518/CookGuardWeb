class CreateSRoleMsgs < ActiveRecord::Migration
  def change
    create_table :s_role_msgs do |t|
      t.string :role_name, null: false
      t.string :valid_flag, null: false
      t.integer :root_distance
      t.integer :queue_index

      t.timestamps null: false
    end
  end
end
