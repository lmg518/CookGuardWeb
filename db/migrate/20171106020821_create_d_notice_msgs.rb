class CreateDNoticeMsgs < ActiveRecord::Migration
  def change
    create_table :d_notice_msgs do |t|
      t.string   "title",        limit: 255
      t.string   "author",       limit: 255
      t.string   "contents",     limit: 255
      t.string   "notice_type",  limit: 255
      t.string   "notice_level", limit: 255
      t.datetime "send_time"
      t.datetime "recive_tiem"

      t.timestamps null: false
    end
  end
end