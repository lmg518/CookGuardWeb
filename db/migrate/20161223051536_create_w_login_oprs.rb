class CreateWLoginOprs < ActiveRecord::Migration
  def change
    create_table :w_login_oprs do |t|
      t.string :op_code, null: false
      t.datetime :op_time, null: false
      t.string :login_no, null: false
      t.string :ip_addr
      t.string :op_note
      t.string :op_describe

      t.timestamps null: false
    end
  end
end
