class CreateSRegionCodes < ActiveRecord::Migration
  def change
    create_table :s_region_codes do |t|
      t.string :region_code, null: false
      t.string :region_name, null: false
      t.integer :s_region_level_id
      t.string :is_leaf

      t.references :father_region

      t.timestamps null: false
    end
  end
end
