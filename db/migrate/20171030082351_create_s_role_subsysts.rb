class CreateSRoleSubsysts < ActiveRecord::Migration
  def change
    create_table :s_role_subsysts do |t|
      t.integer :s_role_msg_id, :null => false
      t.integer :s_subsyst_list_id, :null => false

      t.timestamps null: false
    end
  end
end
