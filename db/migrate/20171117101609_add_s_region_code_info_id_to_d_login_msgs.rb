class AddSRegionCodeInfoIdToDLoginMsgs < ActiveRecord::Migration
  def change
    add_column :d_login_msgs, :s_region_code_info_id, :integer
	add_index :d_login_msgs, :s_region_code_info_id
  end
end
