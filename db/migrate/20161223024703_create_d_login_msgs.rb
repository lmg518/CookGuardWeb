class CreateDLoginMsgs < ActiveRecord::Migration
  def change
    create_table :d_login_msgs do |t|
      t.string :login_no, null:false
      t.string :login_name, null:false
      t.string :password
      t.string :password_confirmation
      t.string :password_digest, null:false
      t.integer :group_id, null:false
      t.integer :s_role_msg_id, null:false
      t.string :valid_flag
      t.date :valid_begin
      t.date :valid_end
      t.string :ip_address
      t.string :describe

      t.timestamps null: false
    end

    add_index :d_login_msgs, :login_no, unique: true

  end
end
