class AddSRegionCodeIdToSRegionCodeInfos < ActiveRecord::Migration
  def change
		add_column :s_region_code_infos, :s_region_code_id, :integer
		add_index :s_region_code_infos, :s_region_code_id
  end
end
