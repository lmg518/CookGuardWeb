class CreateDWetherData < ActiveRecord::Migration
  def change
    create_table :d_wether_data do |t|
      t.integer :station_id
      t.string :station_name
      t.string :real_time
      t.string :prs
      t.string :prs_sea
      t.string :prs_max
      t.string :prs_min
      t.string :tem
      t.string :tem_max
      t.string :tem_min
      t.string :rhu
      t.string :rhu_min
      t.string :vap
      t.string :pre_1h
      t.string :win_d_inst_max
      t.string :win_s_max
      t.string :win_d_s_max
      t.string :win_s_avg_2mi
      t.string :win_d_avg_2mi
      t.string :win_now
      t.string :win_s_inst_max

      t.timestamps null: false
    end
    add_index :d_wether_data, :station_id
    add_index :d_wether_data, [:station_id, :real_time], :name => "station_id_real_time"
  end
end
