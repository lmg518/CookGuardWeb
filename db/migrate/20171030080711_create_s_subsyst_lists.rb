class CreateSSubsystLists < ActiveRecord::Migration
  def change
    create_table :s_subsyst_lists do |t|
      t.string :sys_name
      t.string :sys_url
      t.string :mark

      t.timestamps null: false
    end
  end
end
