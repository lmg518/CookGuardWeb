class CreateSRegionCodeInfos < ActiveRecord::Migration
  def change
    create_table :s_region_code_infos do |t|
      t.string :linkman
      t.string :linkman_tel
      t.string :unit_address
	    t.string :unit_name
      t.string :unit_code
	    t.string :status
      t.string :station_id
	  
	  
      t.timestamps null: false
    end
  end
end
