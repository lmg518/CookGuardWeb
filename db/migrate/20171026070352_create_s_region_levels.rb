class CreateSRegionLevels < ActiveRecord::Migration
  def change
    create_table :s_region_levels do |t|
      t.string :level_name

      t.timestamps null: false
    end
  end
end
