class CreateDWeatherStations < ActiveRecord::Migration
  def change
    create_table :d_weather_stations do |t|
      t.integer :station_id
      t.string :province_name
      t.string :station_name
      t.string :lat
      t.string :lng

      t.timestamps null: false
    end
    add_index :d_weather_stations, :station_id
  end
end
