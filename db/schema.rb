# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180131091925) do

  create_table "d_login_ips", force: :cascade do |t|
    t.string   "login_name", limit: 255
    t.string   "ip",         limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "d_login_msgs", force: :cascade do |t|
    t.string   "login_no",              limit: 255, null: false
    t.string   "login_name",            limit: 255, null: false
    t.string   "password",              limit: 255
    t.string   "password_confirmation", limit: 255
    t.string   "password_digest",       limit: 255, null: false
    t.integer  "group_id",              limit: 4
    t.integer  "s_role_msg_id",         limit: 4
    t.string   "valid_flag",            limit: 255
    t.date     "valid_begin"
    t.date     "valid_end"
    t.string   "ip_address",            limit: 255
    t.string   "describe",              limit: 255
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "session",               limit: 255
    t.integer  "s_region_code_info_id", limit: 4
    t.string   "id_card",               limit: 255
    t.string   "telphone",              limit: 255
    t.string   "email",                 limit: 255
    t.string   "work_num",              limit: 255
    t.string   "work_status",           limit: 255
    t.string   "registration_id",       limit: 255
  end

  add_index "d_login_msgs", ["login_no"], name: "index_d_login_msgs_on_login_no", unique: true, using: :btree
  add_index "d_login_msgs", ["s_region_code_info_id"], name: "index_d_login_msgs_on_s_region_code_info_id", using: :btree

  create_table "d_notice_msgs", force: :cascade do |t|
    t.string   "title",        limit: 255
    t.string   "author",       limit: 255
    t.string   "contents",     limit: 255
    t.string   "notice_type",  limit: 255
    t.string   "notice_level", limit: 255
    t.datetime "send_time"
    t.datetime "recive_tiem"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "d_region_code_infos", force: :cascade do |t|
    t.integer  "s_region_code_id", limit: 4
    t.string   "linkman",          limit: 255
    t.string   "linkman_tel",      limit: 255
    t.string   "unit_address",     limit: 255
    t.string   "unit_name",        limit: 255
    t.string   "unit_code",        limit: 255
    t.string   "status",           limit: 255
    t.string   "station_id",       limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "d_region_code_infos", ["s_region_code_id"], name: "index_d_region_code_infos_on_s_region_code_id", unique: true, using: :btree

  create_table "d_weather_stations", force: :cascade do |t|
    t.integer  "station_id",    limit: 4
    t.string   "province_name", limit: 255
    t.string   "station_name",  limit: 255
    t.string   "lat",           limit: 255
    t.string   "lng",           limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "d_weather_stations", ["station_id"], name: "index_d_weather_stations_on_station_id", using: :btree

  create_table "d_wether_data", force: :cascade do |t|
    t.integer  "station_id",     limit: 4
    t.string   "station_name",   limit: 255
    t.string   "real_time",      limit: 255
    t.string   "prs",            limit: 255
    t.string   "prs_sea",        limit: 255
    t.string   "prs_max",        limit: 255
    t.string   "prs_min",        limit: 255
    t.string   "tem",            limit: 255
    t.string   "tem_max",        limit: 255
    t.string   "tem_min",        limit: 255
    t.string   "rhu",            limit: 255
    t.string   "rhu_min",        limit: 255
    t.string   "vap",            limit: 255
    t.string   "pre_1h",         limit: 255
    t.string   "win_d_inst_max", limit: 255
    t.string   "win_s_max",      limit: 255
    t.string   "win_d_s_max",    limit: 255
    t.string   "win_s_avg_2mi",  limit: 255
    t.string   "win_d_avg_2mi",  limit: 255
    t.string   "win_now",        limit: 255
    t.string   "win_s_inst_max", limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "d_wether_data", ["station_id", "real_time"], name: "station_id_real_time", using: :btree
  add_index "d_wether_data", ["station_id"], name: "index_d_wether_data_on_station_id", using: :btree

  create_table "ihomes", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "s_func_msgs", force: :cascade do |t|
    t.string   "func_code",       limit: 255, null: false
    t.string   "func_name",       limit: 255, null: false
    t.string   "valid_flag",      limit: 255, null: false
    t.integer  "root_distance",   limit: 4,   null: false
    t.integer  "queue_index",     limit: 4,   null: false
    t.string   "func_url",        limit: 255
    t.string   "new_window_flag", limit: 255
    t.string   "func_note",       limit: 255
    t.integer  "father_func_id",  limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "s_region_code_infos", force: :cascade do |t|
    t.string   "linkman",          limit: 255
    t.string   "linkman_tel",      limit: 255
    t.string   "unit_address",     limit: 255
    t.string   "unit_name",        limit: 255
    t.string   "unit_code",        limit: 255
    t.string   "status",           limit: 255
    t.string   "station_id",       limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "s_region_code_id", limit: 4
  end

  add_index "s_region_code_infos", ["s_region_code_id"], name: "index_s_region_code_infos_on_s_region_code_id", using: :btree

  create_table "s_region_codes", force: :cascade do |t|
    t.string   "region_code",       limit: 255, null: false
    t.string   "region_name",       limit: 255, null: false
    t.integer  "s_region_level_id", limit: 4
    t.string   "is_leaf",           limit: 255
    t.integer  "father_region_id",  limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "s_region_levels", force: :cascade do |t|
    t.string   "level_name", limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "s_role_funcs", force: :cascade do |t|
    t.integer  "s_func_msg_id", limit: 4, null: false
    t.integer  "s_role_msg_id", limit: 4, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "s_role_msgs", force: :cascade do |t|
    t.string   "role_name",     limit: 255, null: false
    t.string   "valid_flag",    limit: 255, null: false
    t.integer  "root_distance", limit: 4
    t.integer  "queue_index",   limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "s_role_subsysts", force: :cascade do |t|
    t.integer  "s_role_msg_id",     limit: 4, null: false
    t.integer  "s_subsyst_list_id", limit: 4, null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "s_subsyst_lists", force: :cascade do |t|
    t.string   "sys_name",   limit: 255
    t.string   "sys_url",    limit: 255
    t.string   "mark",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "sessions", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "w_login_oprs", force: :cascade do |t|
    t.string   "op_code",     limit: 255, null: false
    t.datetime "op_time",                 null: false
    t.string   "login_no",    limit: 255, null: false
    t.string   "ip_addr",     limit: 255
    t.string   "op_note",     limit: 255
    t.string   "op_describe", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

end
