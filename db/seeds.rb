# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

role = SRoleMsg.find_or_create_by(:role_name => "超级管理员", :valid_flag => "1", )
DLoginMsg.find_or_create_by(:login_no => "hzf", :password => "hzf", :login_name => "平台管理员" ,:s_role_msg_id => role.id )